# Images to Figure with Markdown

## Configuration

* In the `mkdocs.yml` config file, add the following links to `@rod2ik`'s cdn:

```yaml linenums="0"
extra_css:
   # For mkdocs integration: Add a link to 'massilia.css' cdn by rod2ik
   - https://cdn.jsdelivr.net/gh/rod2ik/cdn@main/mkdocs/css/massilia.css
extra_javascript:
   # For mkdocs integration: Add a link to 'massilia-images.js' cdn by rod2ik
  - https://cdn.jsdelivr.net/gh/rod2ik/cdn@main/mkdocs/javascripts/massilia-images.js
```


That's all folks !

