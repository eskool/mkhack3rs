# Images : Markdown Syntax to HTML Figure Syntax

## Center Image Markdown

=== "Rendering"
    ![Nice Boat Center with `.shadow`](./bateau.jpg){.center .shadow width=80%}

    ![Nice Boat Center without `.shadow`](./bateau.jpg){.center width=80%}

    some quiet long following text
    some quiet long following text
    some quiet long following text
=== ":fa-markdown: Markdown"
    ```markdown
    ![Nice Boat Center with `.shadow`](./bateau.jpg){.center .shadow width=80%}

    ![Nice Boat Center without `.shadow`](./bateau.jpg){.center width=80%}

    some quiet long following text
    some quiet long following text
    some quiet long following text
    ```

## Left Image Markdown

=== "Rendering"
    ![Left with `.shadow`](./bateau.jpg){.left .shadow width=40%}

    ![Left without `.shadow`](./bateau.jpg){.left width=40%}

    some quiet long following text
    some quiet long following text
    some quiet long following text
=== ":fa-markdown: Markdown"
    ```markdown
    ![Left with `.shadow`](./bateau.jpg){.left .shadow width=40%}

    ![Left without `.shadow`](./bateau.jpg){.left width=40%}

    some quiet long following text
    some quiet long following text
    some quiet long following text
    ```

    Note : You can insert the following HTML tag afterwards, in your markdown, to clear the left float
    ```html
    <clear></clear>
    ```
    
    afterwards, to clear the left float.

## Right Image Markdown

=== "Rendering"
    ![Nice Boat Right with `.shadow`](./bateau.jpg){.right .shadow width=40%}

    ![Nice Boat Right without `.shadow`](./bateau.jpg){.right width=40%}

    some quiet long following text
    some quiet long following text
    some quiet long following text
=== ":fa-markdown: Markdown"
    ```markdown
    ![Nice Boat Right with `.shadow`](./bateau.jpg){.right .shadow width=40%}

    ![Nice Boat Right without `.shadow`](./bateau.jpg){.right width=40%}

    some quiet long following text
    some quiet long following text
    some quiet long following text
    ```

    Note : You can insert the following HTML tag afterwards, in your markdown, to clear the right float
    ```html
    <clear></clear>
    ```

## Left & Right Image Markdown

=== "Rendering"
    ![Nice Boat Right](./bateau.jpg){.right width=40%}

    ![Nice Boat Left](./bateau.jpg){.left width=40%}

    some quiet long following text
    some quiet long following text
    some quiet long following text
=== ":fa-markdown: Markdown"
    ```markdown
    ![Nice Boat Right](./bateau.jpg){.right width=40%}

    ![Nice Boat Left](./bateau.jpg){.left width=40%}

    some quiet long following text
    some quiet long following text
    some quiet long following text
    ```

    Note : You can insert the following HTML tag afterwards, in your markdown, to clear the right float
    ```html
    <clear></clear>
    ```

## Image HTML inside Markdown, without Figcaption

=== "Rendering"
    <center>
    <figure class="left">
    <img src="../bateau.jpg">
    </figure>
    </center>

    some quiet long following text
    some quiet long following text
    some quiet long following text
=== ":fa-html5: Html5 inside :fa-markdown: Markdown"
    ```html
    <center>
    <figure class="left">
    <img src="../bateau.jpg">
    </figure>
    </center>

    some quiet long following text
    some quiet long following text
    some quiet long following text
    ```

    Note : 
    
    * You can insert the following HTML tag afterwards, in your markdown, to clear the left/right float

        ```html
        <clear></clear>
        ```

    * other possibles for `figure` : `right`, `center` 
    * other possibles for `img` : `shadow/ombre/encadree`

## Image HTML inside Markdown, with Figcaption

=== "Rendering"
    <center>
    <figure class="right">
    <img src="../bateau.jpg">
    <figcaption>
    figcaption image 2
    </figcaption>
    </figure>
    </center>

    some quiet long following text
    some quiet long following text
    some quiet long following text
=== ":fa-html5: Html5 inside :fa-markdown: Markdown"
    ```html
    <center>
    <figure class="left">
    <img src="../bateau.jpg">
    <figcaption>
    figcaption image 2
    </figcaption>
    </figure>
    </center>

    some quiet long following text
    some quiet long following text
    some quiet long following text
    ```

    Note :

    * You can insert the following HTML tag afterwards, in your markdown, to clear the left/right float

        ```html
        <clear></clear>
        ```

    * other possibles classes : `right`, `center`
    * other possibles for `img` : `shadow/ombre/encadree`

