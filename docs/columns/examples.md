# Multi-Columns in mkdocs

!!! info
    The <bred>RED LINES</bred> displayed in the Rendering tabs hereafter, are just shown to give you an idea of the spread of the columns.  
    They are obviously not displayed.

## Equally Wide Multi-Columns

### Col / Column / Colonne

In every and each following line hereafter, the $3$ keywords `col`, `column` and `colonne` are equivalent and can used one for the other (only `col` will be described).

* `col`
* `column`
* `colonne`

### 2 Columns equally wide with `_2`

An easy syntax to have $2$ columns of the **SAME WIDTH**.

=== ":fa-markdown: Markdown"
    ```markdown linenums="0"
    !!! col _2
        Column 1 of 2
    !!! col _2
        Column 2 of 2
    ```
=== "Rendering"
    !!! col _2
        <div style="border: 2px solid red;">
        Column 1 of 2
        </div>
    !!! col _2
        <div style="border: 2px solid red;">
        Column 2 of 2
        </div>

### 3 Columns equally wide with `_3`

An easy syntax to have $3$ columns of the **SAME WIDTH**.

=== ":fa-markdown: Markdown"
    ```markdown linenums="0"
    !!! col _3
        Column 1 of 3
    !!! col _3
        Column 2 of 3
    !!! col _3
        Column 3 of 3
    ```
=== "Rendering"
    !!! col _3
        <div style="border: 2px solid red;">
        Column 1 of 3
        </div>
    !!! col _3
        <div style="border: 2px solid red;">
        Column 2 of 3
        </div>
    !!! col _3
        <div style="border: 2px solid red;">
        Column 3 of 3
        </div>

### 4 Columns equally wide with `_4`

An easy syntax to have $4$ columns of the **SAME WIDTH**.

=== ":fa-markdown: Markdown"
    ```markdown linenums="0"
    !!! col _4
        Column 1 of 4
    !!! col _4
        Column 2 of 4
    !!! col _4
        Column 3 of 4
    !!! col _4
        Column 4 of 4
    ```
=== "Rendering"
    !!! col _4
        <div style="border: 2px solid red;">
        Column 1 of 4
        </div>
    !!! col _4
        <div style="border: 2px solid red;">
        Column 2 of 4
        </div>
    !!! col _4
        <div style="border: 2px solid red;">
        Column 3 of 4
        </div>
    !!! col _4
        <div style="border: 2px solid red;">
        Column 4 of 4
        </div>

### 5 and 6 Columns equally wide with `_5` and `_6`

The Same syntax applies up to $6$ columns, equally wide :

* $5$ columns : syntax is `_5` (instead of `_2` or `_3` or `_4`)
* $6$ columns : syntax is `_6`

## Different Width Multi-Columns

Columns can also have **different widths**, syntax uses **TWO underscores** followed by an integer valeur representing **a percentage of the parent width**.

=== ":fa-markdown: Markdown"
    ```markdown linenums="0"
    !!! col __17
        17% width
    !!! col __60
        60% width
    !!! col __23
        23% width
    ```
=== "Rendering"
    !!! col __17
        <div style="border: 2px solid red;">
        17% width
        </div>
    !!! col __60
        <div style="border: 2px solid red;">
        60% width
        </div>
    !!! col __23
        <div style="border: 2px solid red;">
        23% width
        </div>

!!! info
    This kind of supposes that the **sum of the widths = 100%**, as a classical use case.  
    
    * If not (sum of widths != 100%), then the default behavior is that the following contents will continue to accumulate after the last column as `float:left;` does in CSS.
    * This can be modified with the `clear` keyword

## `Clear` (after) the last Column

The default behaviour is to accumulate contents (columns or other contents: texts, images, etc..) as `float:left;` after the last column. This can be modified with the `clear` keyword.

### With Clear

=== ":fa-markdown: Markdown"
    ```markdown linenums="0"
    !!! col __18
        18%
    !!! col __27
        27%
    !!! col __10 clear
        10%
    lorem ipsum bla bla bla
    ```
=== "Rendering"
    !!! col __18
        <div style="border: 2px solid red;">
        18% width
        </div>
    !!! col __27
        <div style="border: 2px solid red;">
        27% width
        </div>
    !!! col __10 clear
        <div style="border: 2px solid red;">
        10% width
        </div>
    lorem ipsum bla bla bla

### Without Clear

=== ":fa-markdown: Markdown"
    ```markdown linenums="0"
    !!! col __18
        18%
    !!! col __27
        27%
    !!! col __10
        10%
    lorem ipsum bla bla bla
    ```
=== "Rendering"
    !!! col __18
        <div style="border: 2px solid red;">
        18% width
        </div>
    !!! col __27
        <div style="border: 2px solid red;">
        27% width
        </div>
    !!! col __10
        <div style="border: 2px solid red;">
        10% width
        </div>
    lorem ipsum bla bla bla

## Columns `right`

The previously described default behaviour (to accumulate columns to the left) can be modified with the `right` keyword.

### With Clear

=== ":fa-markdown: Markdown"
    ```markdown linenums="0"
    !!! col __18 right
        18%
    !!! col __27 right
        27%
    !!! col __10 right clear
        10%
    lorem ipsum bla bla bla
    ```
=== "Rendering"
    !!! col __18 right
        <div style="border: 2px solid red;">
        18% width
        </div>
    !!! col __27 right
        <div style="border: 2px solid red;">
        27% width
        </div>
    !!! col __10 right clear
        <div style="border: 2px solid red;">
        10% width
        </div>
    lorem ipsum bla bla bla

### Without Clear

=== ":fa-markdown: Markdown"
    ```markdown linenums="0"
    !!! col __18 right
        18%
    !!! col __27 right
        27%
    !!! col __10 right
        10%
    lorem ipsum bla bla bla
    ```
=== "Rendering"
    !!! col __18 right
        <div style="border: 2px solid red;">
        18% width
        </div>
    !!! col __27 right
        <div style="border: 2px solid red;">
        27% width
        </div>
    !!! col __10 right
        <div style="border: 2px solid red;">
        10% width
        </div>
    lorem ipsum bla bla bla

## Centering content within a Column, with `center`

Centering of texts and contents is possible with the `center` keyword.

### With Center

=== ":fa-markdown: Markdown"
    ```markdown linenums="0"
    !!! col _2 center
        Column 1 of 2
    !!! col _2 center
        ![Boat](bateau.jpg)
    ```
=== "Rendering"
    !!! col _2 center
        <div style="border: 2px solid red;">
        Column 1 of 2
        </div>
    !!! col _2 center
        |A |B|C|D|E|F|G|H|
        |:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
        | a | b | c | $\sqrt 2$ | d | > | $\pi$ | e |
        | > | 1 | ^ |     3     |   | > | ^     | ^ |
        | > | ^ | 2 |     ^     | 5 |   |       |   |
        | > | ^ | ^ |     ^     | ^ |   |       |   |
        | > | ^ | ^ |     6     | 7 |   |       | &nbsp; |

        <div style="border: 2px solid red;">
        ![Boat](bateau.jpg)
        </div>



### Without Center

=== ":fa-markdown: Markdown"
    ```markdown linenums="0"
    !!! col _2
        Column 1 of 2
    !!! col _2
        ![Boat](bateau.jpg)
    ```
=== "Rendering"
    !!! col _2
        <div style="border: 2px solid red;">
        Column 1 of 2
        </div>
    !!! col _2
        |A |B|C|D|E|F|G|H|
        |:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
        | a | b | c | $\sqrt 2$ | d | > | $\pi$ | e |
        | > | 1 | ^ |     3     |   | > | ^     | ^ |
        | > | ^ | 2 |     ^     | 5 |   |       |   |
        | > | ^ | ^ |     ^     | ^ |   |       |   |
        | > | ^ | ^ |     6     | 7 |   |       | &nbsp; |

        <div style="border: 2px solid red;">
        ![Boat](bateau.jpg)
        </div>
        

### center & right

`center` keyword is compatible with the `right` keyword:

=== ":fa-markdown: Markdown"
    ```markdown linenums="0"
    !!! col __18 right center
        18%
    !!! col __27 right center
        27%
    !!! col __10 right center clear
        10%
    lorem ipsum bla bla bla
    ```
=== "Rendering"
    !!! col __18 right center
        <div style="border: 2px solid red;">
        18% width
        </div>
    !!! col __27 right center
        <div style="border: 2px solid red;">
        27% width
        </div>
    !!! col __10 right center clear
        <div style="border: 2px solid red;">
        10% width
        </div>
    lorem ipsum bla bla bla

## Compatibility with Admonitions

=== ":fa-markdown: Markdown"
    ```bash linenums="0"
    !!! col __18
        !!! info
            info admonition with 18% width  
    !!! col __27  
        !!! tip  
            tip admonition with 27% width  
    !!! col __45 clear  
        !!! warning  
            warning admonition with 45% width  
    lorem ipsum bla bla bla  
    ```
=== "Rendering"
    !!! col __18
        !!! info
            info admonition with 18% width
    !!! col __27
        !!! tip
            tip admonition with 27% width
    !!! col __45 clear
        !!! warning
            warning admonition with 45% width
    lorem ipsum bla bla bla