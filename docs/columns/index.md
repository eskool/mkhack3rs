# Multi-Columns for mkdocs

## What is Multi-Columns for mkdocs ?

Multi-Columns for mkdocs take advantage of **admonitions** functionnalities natively provided by the **material theme** for mkdocs, to offer **multi-columns** in mkdocs, in a way that they are compatible with:

* Admonitions themselves
* Markdown tables
* Graphviz and Mermaid Diagrams

## Configuration

In the `mkdocs.yml` config file, add the following:

* In `extra_css`, add a link to rod2ik's cdn `massilia-columns.css`

```yaml linenums="0"
extra_css:
   # For mkdocs integration: Add a link to 'columns.css' cdn by rod2ik
   - https://cdn.jsdelivr.net/gh/rod2ik/cdn@main/mkdocs/css/massilia-columns.css
```

That's all folks !
