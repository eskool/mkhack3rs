# Graphviz

## Kroki

```kroki-graphviz no-transparency=false
graph G {
  size="1.8"
  node [shape=circle, fontname="fira code", fontsize=28, penwidth=3]
  edge [penwidth=3]
  "-" -- {"x", "+"} 
  "x" -- 3 [weight=2]
  "x" -- 5
  "+" -- 2
  "+" -- 4 [weight=2]
}
```

## Graphviz SVG's

=== "Trees / Arbres"
    <center>

    ```dot
    graph G {
      size="1.8"
      node [shape=circle, fontname="fira code", fontsize=28, penwidth=3]
      edge [penwidth=3]
      "-" -- {"x", "+"} 
      "x" -- 3 [weight=2]
      "x" -- 5
      "+" -- 2
      "+" -- 4 [weight=2]
    }
    ```

    <figcaption><b>Arbre 1 : Arbre des Expressions Arithmétiques</b></figcaption>

    ```dot
    graph G {
      size="3"
      forcelabels=true
      node [shape=circle, fontname="fira code", fontsize=24, penwidth=3];
      edge [penwidth=3]
      5 -- {4, 32} [color=red]
      31, 32 [label="3"]
      21, 22, 23 [label="2"]
      11, 12, 13, 14, 15 [label="1"]
      01, 02, 03, 04, 05, 06, 07, 08 [label="0"]
      4 -- 31 [color=blue, weight=2]
      4 -- 22 [color=blue]
      31 -- 21 [color=red, weight=3]
      31 -- 12 [color=red]
      32 -- 23 [color=blue]
      32 -- 15 [color=blue, weight=2]
      21 -- 11 [color=blue, weight=3]
      21 -- 02 [color=blue]
      11 -- 01 [color=red]
      12 -- 03 [color=blue]
      22 -- 13 [color=red]
      22 -- 05 [color=red, weight=2]
      13 -- 04 [color=blue]
      23 -- {14, 07} [color=red]
      14 -- 06 [color=blue]
      15 -- 08 [color=red]
      07, 08 [shape=doublecircle]
    }
    ```

    <figcaption><b>Arbre 2 : Arbre de Jeu (de Nim)</b></figcaption>

    ```dot
    graph Code {
      size="2.9"
      node [penwidth=2, fontsize=38]
      edge [penwidth=2]
      {i, Range, Body2, a2}
      Body1, Body2, Assign1, Assign2, For, Print, Range, "\*" [shape=rectangle]
      Body1, Body2 [label="Body", color=red, fontcolor=red]
      Assign1, Assign2, For, Print, Range, "\*" [color=blue, fontcolor=blue]
      Assign1, Assign2 [label="Assign"]
      i, a1, a2, a3, a4, 2, 5, 3 [color="#08a82d", fontcolor="#08a82d", shape=circle]
      a1, a2, a3, a4 [label="a"]
      Body1 -- {Assign1, For, Print}
      Assign1 -- {a1, 2}
      For -- {i, Range, Body2}
      Range -- 3
      Body2 -- Assign2
      Assign2 -- {a2, "\*"}
      "\*" -- {a3, 5}
      Print -- a4
    }
    ```

    <figcaption><b>Arbre 3 : Arbre Syntaxique du code ci-dessous</b></figcaption>

    ```dot
    graph G {
      size="4.9"
      node [shape=circle, fontsize=34, penwidth=2]
      edge [penwidth=2]
      r
      r -- a
      r -- b
      subgraph cluster01 {
        c
        d
        e
        label="Noeuds Frères"
        fontsize=38
      }
      a -- c
      a -- d
      a -- e
      b -- f
      subgraph cluster02 {
        f [color=red, fontcolor=red, style=bold]
        label="Feuilles"
        fontsize=34
        color=none
        fontcolor=red
      }
      subgraph cluster03 {
        r [color=red, fontcolor=red, style=bold]
        label="Noeuds Internes"
        fontsize=34
        color=none
        fontcolor=blue
      }
      e -- o
      e -- p
      c -- {i, j}
      subgraph cluster04 {
        i
        label="Père de k, de l, et de m"
        fontsize=34
      }
      i -- {k, l ,m}
      subgraph cluster05 {
        k
        l
        m
        label="Les 3 Fils de i"
        fontsize=34
      }
      k -- q -- s
      m -- {u, t}
      r, a, b, c, e, i, j, k, l, m, q [color=blue, fontcolor=blue, penwidth=3]
      j, l, d, o, p, s, u, t  [color=red, fontcolor=red, style=bold]
    }
    ```

    <figcaption>Noeuds Internes vs Noeuds Externes/Feuilles.<br/>Noeud Père et Noeuds Fils.<br/>Noeuds Frères</figcaption>

    ```dot
    graph G {
      splines=false
      size="3.5"
      node [shape=circle, fontsize=28, penwidth=3]
      edge [penwidth=3]

      subgraph cluster00 {
        a
        color=red
        fontcolor=red
        fontsize=28
        label="arité 4"
      }
      subgraph cluster01 {
        b
        color=blue
        fontcolor=blue
        fontsize=28
        label="arité 2"
      }
      a -- {b, c, d, e} [color=red]
      subgraph cluster02 {
        e
        color=deeppink
        fontcolor=deeppink
        fontsize=28
        label="arité 3"
      }
        subgraph cluster03 {
        h
        color="#22CC22"
        fontcolor="#22CC22"
        fontsize=28
        label="arité 0"
        style=bold
      }
      b -- {f, g} [color=blue]
      e -- {h, i, j} [color=deeppink]
      f, g, c, d, h, i, j [color="#22CC22", fontcolor="#22CC22"]
    }
    ```

    <figcaption>Arbre 4-aire</figcaption>

    ```dot
    graph G {
      size="3"
      //splines=false
      node [penwidth=3, shape=circle, fontsize=32]
      edge [penwidth=3, fontsize=28]
      r -- a [label="Chemin", color=red, fontcolor=red]
      r -- b
      a -- c [label="Arête", color=blue, fontcolor=blue]
      a -- d 
      a -- e [color=red, fontcolor=red]
      b -- f
      e -- g [label="Arête", color=blue, fontcolor=blue]
      e -- h [color=red, fontcolor=red]
      r, a, e, h [color=red, fontcolor=red]
    }
    ```

    <figcaption>Chemin de la racine <em>r</em> vers <em>h</em><br/>Longueur 3</figcaption>

    ```dot
    graph G {
      size="4"
      node [shape=circle, fontsize=20, label="     "]
      subgraph cluster00 {
        r
        fontsize=24
        label="Niveau 0"
        labeljust="r"
      }
      subgraph cluster01 {
        b, a
        fontsize=24
        label="Niveau 1"
        labeljust="r"
      }
      subgraph cluster02 {
        f, e, d, c
        fontsize=24
        label="Niveau 2"
        labeljust="r"
      }
      subgraph cluster03 {
        h, g
        fontsize=24
        label="Niveau 3"
        labeljust="r"
      }
      r -- a
      r -- b
      a -- c
      a -- d
      a -- e
      b -- f
      e -- g
      e -- h
    }
    ```

    ```dot
    graph ABR {
      size="10"
      node [fontcolor=none, shape=circle, fixedsize=true, width=0.8,fontsize=38, penwidth=3]
      //node [shape=circle, fixedsize=true, width=0.8,fontsize=38, penwidth=3]
      edge [penwidth=3]
      1 -- {2, 3}
      2 -- v1 [color=none]
      2 -- 4 
      2 -- 5
      3 -- 6
      3 -- 7
      3 -- v2 [color=none]
      4 -- v3 [color=none]
      4 -- 8 [weight=2]
      4 -- 9
      5 -- 10
      5 -- 11
      5 -- v4 [color=none]
      8 -- 16 [weight=4]
      8 -- 17
      6 -- 12 [weight=3]
      6 -- 13 [weight=2]
      7 -- 14
      7 -- 15 [weight=3]
      9 -- 18
      9 -- 19 [weight=2]
      10 -- 20 [weight=2]
      10 -- 21
      11 -- v5 [color=none]
      11 -- v6 [color=none]
      12 -- 22 [weight=3]
      12 -- 23 [weight=4]
      16 -- {24, 25}
      v1, v2, v3, v4, v5, v6 [color=none, fontcolor=none, label=""]
    }
    ```

    ```dot
    graph ROT {
      size=4
      node [shape=circle, fontname="fira code"]
      z -- q
      q -- p
      q -- C
      p -- A, B
      p, q [style=filled]
      p [fillcolor=green]
      q [fillcolor=red]
      A, B, C [shape=triangle]
      z [color=none, fontcolor=none]
    }
    ```

    ```dot
    digraph AVL {
      size=3
      node [shape=circle, fontname="fira code", fontsize=30, penwidth=3]
      edge [penwidth=3, dir=none]
      10 -> 8 [xlabel="<10", dir=forward, color=red, fontsize=30, fontcolor=red, arrowhead=vee]
      10 -> 8
      10 -> 20 [weight=2]
      8 -> 6 [xlabel="<8", dir=forward, color=red, fontsize=30, fontcolor=red, arrowhead=vee, weight=2]
      8 -> 6
      8 -> v1 [color=none]
      20 -> 18 [weight=2]
      20 -> 23 [weight=4]
      23 -> v4 [color=none]
      23 -> 25 [weight=3]
      18 -> v2 [color=none, weight=2]
      6 -> 5 [xlabel="<6", dir=forward, color=red, fontsize=30, fontcolor=red, arrowhead=vee]
      6 -> 5 [color="#C4EDFF", penwidth=6, weight=2]
      6 -> v3 [color=none, weight=3]
      6 [xlabel="-1", shape=doublecircle, style=filled, fillcolor=red, color=red]
      5 [xlabel="0", shape=doublecircle, style=filled, fillcolor="#C4EDFF", color="#C4EDFF"]
      8 [xlabel="-2", shape=doublecircle, style=filled, fillcolor=green, color=green]
      10 [xlabel="0"]
      8 [xlabel="-2"]
      20 [xlabel="+1"]
      6 [xlabel="-1"]
      18 [xlabel="0"]
      23 [xlabel="+1"]
      25 [xlabel="0"]
      v1, v2, v3, v4 [color=none, fontcolor=none, label=""]
    }
    ```

    ```dot
    digraph AVL {
      size=4
      node [shape=circle, fontname="fira code", fontsize=30, penwidth=3]
      edge [penwidth=3, dir=none]
      10 -> 8 [weight=2]
      10 -> 20
      10 -> 20 [xlabel=">10", dir=forward, color=red, fontsize=30, fontcolor=red, arrowhead=vee]
      8 -> 6 [weight=3]
      8 -> v1 [color=none]
      20 -> 18
      20 -> 23 [weight=2] 
      20 -> 23 [xlabel=">20", dir=forward, color=red, fontsize=30, fontcolor=red, arrowhead=vee]
      23 -> v4 [color=none]
      23 -> 25
      23 -> 25 [xlabel=">23", dir=forward, color=red, fontsize=30, fontcolor=red, arrowhead=vee]
      25 -> 24 [color="#C4EDFF", penwidth=6, weight=2]
      25 -> 24 [xlabel="<25", dir=forward, fontsize=30, fontcolor=red, arrowhead=vee, color=red, penwidth=6]
      25 -> v5 [color=none, weight=4]
      18 -> v2 [color=none, weight=2]
      6 -> v3 [color=none]
      25 [xlabel="-1", shape=doublecircle, style=filled, fillcolor=red, color=red]
      24 [xlabel="0", shape=doublecircle, style=filled, fillcolor="#C4EDFF", color="#C4EDFF"]
      23 [xlabel="-2", shape=doublecircle, style=filled, fillcolor=green, color=green]
      10 [xlabel="+2"]
      8 [xlabel="-1"]
      20 [xlabel="+2"]
      6 [xlabel="0"]
      18 [xlabel="0"]
      23 [xlabel="+2"]
      v1, v2, v3, v4, v5 [color=none, fontcolor=none, label=""]
    }
    ```

    ```dot
    digraph ABR {
      size="6"
      //splines=false
      node [shape=circle, style=filled, fillcolor="#CFFDFF", fixedsize=true, width=0.8, fontsize=38, penwidth=0]
      edge [fontsize=38, penwidth=3, dir=none]
      34 -> 22 [weight=2]
      34 -> 66
      34 -> 66 [xlabel=">34", dir=forward, color=red, fontcolor=red, arrowhead=vee]
      22 -> 7 [weight=3]
      22 -> 29
      7 -> v1 [weight=4, color=none]
      7 -> 17
      17 -> 9 [weight=4]
      17 -> 21
      9 -> 8, 16
      29 -> 25 [weight=2]
      29 -> 32 [weight=3]
      25 -> 23 [weight=3]
      25 -> 28
      28 -> 27
      28 -> v2 [weight=2]
      27 -> 26
      27 -> v3
      32 -> 30
      32 -> v4 [weight=4]
      66 -> 50 [weight=2]
      66 -> 71
      66 -> 71 [label=">66", dir=forward, color=red, fontcolor=red, arrowhead=vee, weight=3]
      50 -> 37 [weight=4]
      50 -> 56 [weight=2]
      37 -> 36 [weight=4]
      37 -> 44
      36 -> 35
      36 -> v5
      56 -> 55
      56 -> v6 [weight=3]
      55 -> 52 
      55 -> v7
      52 -> 51
      52 -> v12
      71 -> 70
      71 -> 70 [label="<71", dir=forward, color=red, fontcolor=red, arrowhead=vee]
      71 -> 81 [weight=5]
      70 -> 68 [weight=2]
      70 -> v9 [weight=1]
      68 -> 67 [weight=2]
      68 -> 69 [weight=3]
      81 -> 80
      81 -> 97 [weight=6]
      97 -> 94
      97 -> v10
      94 -> 88
      94 -> v11
      70 [shape=doublecircle, fillcolor=red, color=red, fontcolor=white, penwidth=2]
      68 [shape=doublecircle, fillcolor=deeppink, color=red, fontcolor=white, penwidth=2]
      v1, v2, v3, v4, v5, v6, 
      v7, v8, v9, v10, v11, v12,v13 [color=none, fontcolor=none, style=none, label=""]
    }
    ```

    <figcaption>ABR <b>quelconque</b><br/>BUT : Supprimer <span style="background-color:#FF0000;color:white;font-weight:700;border-radius:50%;padding:0.3em;">70</span><br/>
    :one: Recherche de <span style="background-color:#FF0000;color:white;font-weight:700;border-radius:50%;padding:0.3em;">70</span> <br/>:two: Déterminer <span style="background-color:#FF00FF;color:white;font-weight:700;border-radius:50%;padding:0.3em;">68</span> = son unique enfant <br/>:three: On supprime le noeud</figcaption>

    </center>

=== "Graphs / Graphes"
    <center>
    ```dot
    graph G {
      size="2";
      bgcolor=none;
      node [shape=circle, fontsize=30, style=bold];
      edge [style=bold, fontsize=30]
      4 -- 0
      4 -- 0
      4 -- 1
      0 -- 2
      0 -- 2
      0 -- 2
      2 -- 2
      0 -- 3
      1 -- 2
      1 -- 3
      1 -- 1
    }
    ```
    <figcaption>Un Graphe Non Pondéré<br/>Non Orienté<br/>Multigraphe (avec boucles)</figcaption>

    ```dot
    graph G {
      size="2";
      bgcolor=none;
      node [shape=circle, fontsize=30];
      0, 2, 1, 4 [color=red, style=bold]
      4 -- 0
      4 -- 1 [color=red, style=bold]
      0 -- 2 [color=red, style=bold]
      0 -- 3
      1 -- 2 [color=red, style=bold]
      1 -- 3
    }
    ```
    <figcaption>Une Chaîne de<br/>Longueur 3</figcaption>

    ```dot
    digraph G {
        size="8"
        node [shape=circle, fontsize=38]
        edge [arrowhead=none]
        subgraph cluster0 {
          0 -> 1
          2, 0, 3, 8, 11, 10 [color=blue, style=bold]
          0 -> 2 [color=blue, style=bold]
          0 -> 3 [color=blue, style=bold]
          2 -> 3
          8 -> 9
          2 -> 9
          8 -> 3 [color=blue, style=bold]
          0 -> 4 -> 10
          10 -> 11 [color=blue, style=bold]
          8 -> 11 [color=blue, style=bold]
          subgraph cluster00 {
            1, 5, 6 [color=red, style=bold]
            1 -> 5 [color=red, style=bold]
            1 -> 6 [color=red, style=bold]
            5 -> 6 [color=red, style=bold]
            label="cycle"
            fontcolor=red
            fontsize=38
            {rank = same; 5; 6;}
          }
        6 -> 7
        2 -> 8
        3 -> 9
        label = "Composante Connexe";
        fontsize=38
        {rank = same; 2; 3;}
        {rank = same; 8; 9;}
        }

        subgraph cluster1 {
        15 -> 16 -> 17
        16 -> 18 -> 19
        label="Composante Connexe"
        fontsize=38
        }

        subgraph cluster2 {
        20 -> 21 -> 22
        20 -> 23
        21 -> 23
        23 -> 24
        label="Composante Connexe"
        fontsize=38
        }
    }
    ```

    <figcaption>
    Graphe G Non Orienté <bpurple>Non Pondéré</bpurple> Non Connexe<br/>
    et <bblue>Une Chaîne de 2 à 10 de longueur 5</bblue>
    </figcaption>

    ```dot
    digraph G {
      size="8"
      node [shape=circle, fontsize=38]
      edge [arrowhead=none, fontsize=38]
      subgraph cluster0 {
        0 -> 1 [label="4"]
        2, 0, 3, 8, 11, 10 [color=blue, fontcolor=blue, style=bold]
        0 -> 2 [label="5", color=blue, fontcolor=blue, style=bold]
        0 -> 3 [label="0.8"color=blue, fontcolor=blue, style=bold]
        2 -> 3 [label="6"]
        8 -> 9 [label="0.4"]
        2 -> 9 [label="8"]
        8 -> 3 [label="2", color=blue, fontcolor=blue, style=bold]
        0 -> 4 [label="7"]
        4 -> 10 [label="6"]
        10 -> 11 [label="5", color=blue, fontcolor=blue, style=bold]
        8 -> 11 [label="4", color=blue, fontcolor=blue, style=bold]
        subgraph cluster00 {
          1, 5, 6 [color=red, style=bold]
          1 -> 5 [label="2", color=red, fontcolor=red, style=bold]
          1 -> 6 [label="3", color=red, fontcolor=red, style=bold]
          5 -> 6 [label="9", color=red, fontcolor=red, style=bold]
          label="cycle"
          fontcolor=red
          fontsize=38
          {rank = same; 5; 6;}
        }
        6 -> 7 [label="4"]
        2 -> 8 [label="3"]
        3 -> 9 [label="6"]
        label = "Composante Connexe";
        fontsize=38
        {rank = same; 2; 3;}
        {rank = same; 8; 9;}
        }

        subgraph cluster1 {
        15 -> 16 [label="1"]
        16 -> 17 [label="4"]
        16 -> 18 [label="2"]
        18 -> 19 [label="3"]
        label="Composante Connexe"
        fontsize=38
        }

        subgraph cluster2 {
        20 -> 21 [label="3"]
        21 -> 22  [label="4"]
        20 -> 23 [label="5"]
        21 -> 23 [label="6"]
        23 -> 24 [label="7"]
        label="Composante Connexe"
        fontsize=38
        }
    }
    ```

    <figcaption>
    Graphe G Non Orienté <bpurple>Pondéré</bpurple> Non Connexe<br/>
    et <bblue>Une Chaîne de 2 à 10, de poids total =5+0,8+2+4+5=16,8</bblue>
    </figcaption>

    ```dot
    digraph G {
      size="2";
      rankdir=LR
      bgcolor=none;
      node [shape=circle, fontsize=38];
      edge [penwidth=2]
      2, 1, 4, 3 [color=red, style=bold]
      0 -> 5
      0 -> 2
      1 -> 5
      4 -> 2 [color=red, style=bold]
      1 -> 3 [color=red, style=bold]
      3 -> 4
      3 -> 0
      2 -> 1 [color=red, style=bold]
      6 -> 3
      {rank = same; 0; 2; 3;}
      {rank = same; 5; 1; 4;}
    }
    ```

    <figcaption>Un Chemin (Orienté) <br/>de Longueur 3</figcaption>

    ```dot
    digraph G {
      size="2";
      rankdir=LR
      bgcolor=none;
      node [shape=circle, color=red, style=bold, fontsize=38];
      edge [color=red, style=bold]
      6, 0, 2, 1, 3
      0, 1, 3, 4, 2
      8 [color=blue, style=bold]
      9 [color=cyan2, style=bold]
      0 -> 2
      1 -> 5
      4 -> 2
      1 -> 3
      4 -> 3
      3 -> 0
      2 -> 1
      5 -> 4
      6 -> 3
      2 -> 6
      5 -> 7
      7 -> 4
      4 -> 8 [color=blue]
      7 -> 8 [color=blue]
      9 -> 1 [color=cyan2]
      {rank = same; 0; 2; 3;}
      {rank = same; 5; 1; 4;}
      {rank = same; 8; 7;9;}
    }
    ```

    <figcaption>Graphe <bred>NON</bred><br/>Fortement Connexe</figcaption>

    ```dot
    digraph G {
      size="2";
      rankdir=LR
      bgcolor=none;
      node [shape=circle, fontsize=38];
      7, 8, 9 [color="#FF00FF", style=bold]
      7 -> 8 -> 9 -> 7 [color="#FF00FF", style=bold]
      9 -> 10
      10 [color=cyan2, style=bold]

      0, 2, 1, 3 
      0, 1, 3, 4, 2 [color=red, style=bold]
      0 -> 5
      0 -> 2 [color=red, style=bold]
      1 -> 5
      4 -> 2 [color=red, style=bold]
      1 -> 3 [color=red, style=bold]
      3 -> 4 [color=red, style=bold]
      3 -> 0 [color=red, style=bold]
      2 -> 1 [color=red, style=bold]
      6 -> 3
      5 [color=purple, style=bold]
      6 [color=blue, style=bold]
      {rank = same; 0; 2; 3;}
      {rank = same; 5; 1; 4;}
    }
    ```

    <figcaption>Chaque couleur<br/>représente une<br/>Composante <br/>Fortement Connexe</figcaption>

    ```dot
    digraph G {
        size="6.9"
        node [shape=circle, fontsize=38, style=bold]
        0 -> 1
        0 -> 2
        0 -> 3
        4 [color=deepskyblue]
        0 -> 4 -> 10
        subgraph clusterCompoConnexe0 {
          node [color=blue]
          edge [color=blue]
          2, 3 [color=blue]
          9 -> 8
          2 -> 3
          2 -> 9
          8 -> 3
          8 -> 2
          3 -> 9
          {rank = same; 2; 3;}
          {rank = same; 8; 9;}
          label="Composante Forte"
          fontcolor=blue
          fontsize=38
        }
        10 [color=gold2]
        11 [color=firebrick2]
        10 -> 11
        8 -> 11

        subgraph clusterCircuit {
          1, 5, 6 [color=red]
          1 -> 5 [color=red]
          5 -> 6 [color=red]
          6 -> 1 [color=red]
          label="circuit"
          fontcolor=red
          fontsize=38
          {rank = same; 5; 6;}
        }
        7 [color=chartreuse, style=bold]
        6 -> 7

        subgraph clusterCompoConnexe1 {
          node [color="#FF00FF"]
          edge [color="#FF00FF"]
          15 -> 16 -> 17 -> 19 -> 15
          16 -> 18 -> 19
          label="Composante Forte"
          fontcolor="#FF00FF"
          fontsize=38
          {rank = same; 15; 16; 17;}
        }

        20 [color=darkgreen, style=bold]
        21 [color=darkorange, style=bold]
        22 [color=cyan, style=bold]
        23 [color="#FF1493", style=bold]
        24 [color="#11FF11", style=bold]
        20 -> 21 -> 22
        20 -> 23
        21 -> 23
        23 -> 24
    }
    ```

    <figcaption>
    Graphe G Orienté <bpurple>Non Pondéré,</bpurple><br/>
    Non Connexe, Non Fortement Connexe,<br/>
    et <bblue>Un Chemin 2 -> 3 -> 9 -> 8 -> 11 de longueur 4</bblue>
    </figcaption>

    ```dot
    digraph G {
        size="6.8"
        node [shape=circle, fontsize=38, style=bold]
        edge [fontsize=38]
        0 -> 1 [label="4"]
        0 -> 2 [label="5"]
        0 -> 3 [label="0.8"]
        4 [color=deepskyblue]
        0 -> 4 [label="7"]
        4 -> 10 [label="6"]
        subgraph clusterCompoConnexe0 {
          node [color=blue]
          edge [color=blue]
          2, 3 [color=blue]
          9 -> 8 [label="4"]
          2 -> 3 [label="5"]
          2 -> 9 [label="7"]
          8 -> 3 [label="8"]
          8 -> 2 [label="3"]
          3 -> 9 [label="5"]
          {rank = same; 2; 3;}
          {rank = same; 8; 9;}
          label="Composante Forte"
          fontcolor=blue
          fontsize=38
        }
        10 [color=gold2]
        11 [color=firebrick2]
        10 -> 11 [label="8"]
        8 -> 11 [label="2"]

        subgraph clusterCircuit {
          edge [color=red]
          1, 5, 6 [color=red]
          1 -> 5 [label="3"]
          5 -> 6 [label="1"]
          6 -> 1 [label="2"]
          label="circuit"
          fontcolor=red
          fontsize=38
          {rank = same; 5; 6;}
        }
        7 [color=chartreuse, style=bold]
        6 -> 7 [label="2"]

        subgraph clusterCompoConnexe1 {
          node [color="#FF00FF"]
          edge [color="#FF00FF"]
          15 -> 16 [label="4"]
          16 -> 17 [label="2"]
          17 -> 19 [label="1"]
          19 -> 15 [label="3"]
          16 -> 18 [label="5"] 
          18 -> 19 [label="7"]
          label="Composante Forte"
          fontcolor="#FF00FF"
          fontsize=38
          {rank = same; 15; 16; 17;}
        }

        20 [color=darkgreen, style=bold]
        21 [color=darkorange, style=bold]
        22 [color=cyan, style=bold]
        23 [color="#FF1493", style=bold]
        24 [color="#11FF11", style=bold]
        20 -> 21 [label="3"]
        21 -> 22 [label="5"]
        20 -> 23 [label="4"]
        21 -> 23 [label="8"]
        23 -> 24 [label="9"]
    }
    ```

    <figcaption>
    Graphe G Orienté <bpurple>Pondéré</bpurple><br/>Non Connexe, Non Fortement Connexe<br/>
    et <bblue>Un Chemin 0 -> 1 -> 5 -> 6 -> 7, de poids total

    $=4+3+1+2=10$

    </bblue>
    </figcaption>

    </center>


## Graphviz inside Admonitions

???- note "graphviz in UnExpanded Block" 

    ```dot
    digraph G {
        rankdir=LR
        Earth [peripheries=2]
        Mars
        Earth -> Mars
    }
    ```

???+ note "graphviz in Expanded Block" 

    ```dot
    digraph G {
        rankdir=LR
        Earth [peripheries=2]
        Mars
        Earth -> Mars
    }
    ```

## Graphviz PNG's (lower quality than SVGs)

=== "Graphviz Render"

    ```dot
    digraph G {
        rankdir=LR
        Earth [peripheries=2]
        Mars
        Earth -> Mars
    }
    ```

=== "Example of Code Syntax"

    **SYNTAX (WATCHOUT) :** NO SPACES BETWEEN ` ``` ` and `dot`

    ~~~
    ``` dot name.png
    digraph G {
        rankdir=LR
        Earth [peripheries=2]
        Mars
        Earth -> Mars
    }
    ```
    ~~~

