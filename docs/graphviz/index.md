# The `mkdocs-graphviz` Extension

<center>

```dot
digraph "example-graphviz" {
     rankdir="LR";
     graph [fontname="Verdana", fontsize="12"];
     node [fontname="Verdana", fontsize="12"];
     edge [fontname="Sans", fontsize="9"];

     sphinx [label="MkDocs", shape="component",
               URL="https://www.mkdocs.org/",
               target="_blank"];
     dot [label="GraphViz", shape="component",
          URL="https://www.graphviz.org/",
          target="_blank"];
     docs [label="Docs (.md)", shape="folder",
          fillcolor=green, style=filled];
     svg_file [label="SVG Image", shape="note", fontcolor=white,
               fillcolor="#3333ff", style=filled];
     html_files [label="HTML Files", shape="folder",
          fillcolor=yellow, style=filled];

     docs -> sphinx [label=" parse "];
     sphinx -> dot [label=" call ", style=dashed, arrowhead=none];
     dot -> svg_file [label=" draw "];
     sphinx -> html_files [label=" render "];
     svg_file -> html_files [style=dashed];
}
```

</center>

## Installation

### Graphviz package must be installed first

!!! info "Graphviz Package must be installed first"
    Please note that you **must** have the Graphviz binaries installed in your machine **at first**.  
    e.g. on Manjaro Linux:

    ```bash linenums="0"
    $ sudo pacman -S graphviz
    ```

### Installation of `mkdocs-graphviz`

Install the [`mkdocs-graphviz`](https://pypi.org/project/mkdocs-graphviz/) extension via `pip`:

* Installation : `$ pip install mkdocs-graphviz`

or

* Upgrade : `$ pip install --upgrade mkdocs-graphviz`

## Configuration

### Activation in `mkdocs.yml`

Activate the `mkdocs_graphviz` extension. For example, with **Mkdocs**, you add a
stanza to `mkdocs.yml`:

```yaml
markdown_extensions:
    - mkdocs_graphviz

extra_javascript:
  - https://cdn.jsdelivr.net/gh/rod2ik/cdn@main/mkdocs/javascripts/massilia-graphviz.js
```

### Configuration Options

**Optionnally**, use any (or a combination) of the following options with all colors being written as:

* a **standard HTML Color Name** as in [this W3C page](https://www.w3schools.com/tags/ref_colornames.asp) (All Caps Allowed)
* an **HTML HEXADECIMAL COLOR, WITHOUT THE # SIGN**

```yaml
markdown_extensions:
    - mkdocs_graphviz:
        light_theme: 000000      # Any HTML Color Name or any HTML Hexadecimal color code WITHOUT the `#` sign
        dark_theme: FFFFFF       # Any HTML Color Name or any HTML Hexadecimal color code WITHOUT the `#` sign
        color: 789ABC            # Any HTML Color Name or any HTML Hexadecimal color code WITHOUT the `#` sign
        bgcolor: none            # Any HTML Color Name or any HTML Hexadecimal color code WITHOUT the `#` sign
        graph_color: 789ABC      # Any HTML Color Name or any HTML Hexadecimal color code WITHOUT the `#` sign
        graph_fontcolor: 789ABC  # Any HTML Color Name or any HTML Hexadecimal color code WITHOUT the `#` sign
        node_color: 789ABC       # Any HTML Color Name or any HTML Hexadecimal color code WITHOUT the `#` sign
        node_fontcolor: 789ABC   # Any HTML Color Name or any HTML Hexadecimal color code WITHOUT the `#` sign
        edge_color: 789ABC       # Any HTML Color Name or any HTML Hexadecimal color code WITHOUT the `#` sign
        edge_fontcolor: 789ABC   # Any HTML Color Name or any HTML Hexadecimal color code WITHOUT the `#` sign
```

Where:

* `light_theme` (default `000000`) is the **default color of the graph (nodes and edges) in Light Theme** in Mkdocs
* `dark_theme` (default `FFFFFF`) is the **default color of the graph (nodes and edges) in Dark Theme** in Mkdocs
* `color` (default `789ABC`) is a color option that modifies **ALL** the following colors **IN BOTH THEMES (Light and Dark)** in just one parameter:
    * All Nodes
    * All Texts inside Nodes
    * All Edges
    * All Labels aside Edges
    FORMAT
* `bgcolor` (default `none`) sets :
    * the background color to be transparent (by default, which is equivalent to `bgcolor: none`), or
    * the background color of the graph
* `graph_color` (default `789ABC`) sets the color of all Subgraphs/Clusters Roundings
* `graph_fontcolor` (default `789ABC`) sets the color of all Subgraphs/Clusters Titles 
* `node_color` (default `789ABC`) sets the color of all Nodes
* `node_fontcolor` (default `789ABC`) sets the color of all Texts inside Nodes
* `edge_color` (default `789ABC`) sets the color of all Edges
* `edge_fontcolor` (default `789ABC`) sets the color of all Labels aside Edges

### Color Codes

Color Codes can be :

* a **standard HTML Color Name** as in [this W3C page](https://www.w3schools.com/tags/ref_colornames.asp) (All Caps Allowed)
* an **HTML HEXADECIMAL COLOR, WITHOUT THE # SIGN**

### Mixing & Conflicting Options

* It is possible to define a general color of the graph with the `color` option, and then overwrite some of the values with the other options (you choose)
* Colors defined with the options can always be overwritten as a **per Node basis**, or a **per Edge basis** directly inside of the graphviz/dot syntax
* `color` option takes precedence over `light_theme` and `dark_theme` options, but not over other options

## Complete Documentation

See the Repository Page : [mkdocs-graphviz on GitLab](https://gitlab.com/rodrigo.schwencke/mkdocs-graphviz)

