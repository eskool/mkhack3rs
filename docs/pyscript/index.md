# PyScript Integration

Reference : [pyscript.net](https://pyscript.net/)

## Configure `mkdocs.yml`

In `mkdocs.yml` :

```yaml
theme:
  name: material
  custom_dir: overrides

extra_css:
    - https://pyscript.net/latest/pyscript.css
```

## Create a `pyscript.html` file

In the `overrides/` directory, besides `main.html`, create a `pyscript.html` file :

```html
{% raw %}
{% extends "main.html" %}

{% block scripts %}
    {{ super() }}
    <script defer src="https://pyscript.net/latest/pyscript.js"></script>
{% endblock %}
{% endraw %}
```

## Per page front-matter

In ANY `.md` file in which you want to activate `PyScript`, place a front-matter:

```yaml
---
template: pyscript.html
---
```

## Add PyScript Code

Then in ANY `.md` file that contains a `template: pyscript.html` front-matter, add any PyScript code as usual, for example :

```html
<div>
<py-script>
    print("Hello World")
</py-script>
<br/>
<p><strong>Today is <label id='date'></label></strong></p>
<py-script>
import time
pyscript.write('date', time.strftime('%d/%m/%Y %H:%M:%S'))
</py-script>
<br/>

<py-script>  
    print("Let's evaluate π :")
    def eval_pi(n):
        pi = 2
        for i in range(1,n):
            pi *= 4 * i ** 2 / (4 * i ** 2 - 1)
        return pi
    pi = eval_pi(100000)
    s = "&nbsp;" * 10 + f"π is approximately {pi:.5f}"
    print(s)
</py-script>
<br/>

<div id="hey"></div>
<py-script>
pyscript.write('hey', f'Hey Mate !')
</py-script>
</div>
```

