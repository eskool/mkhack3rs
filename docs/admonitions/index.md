# Massilia Admonitions

## What are Massilia Admonitions for mkdocs?

Massilia Admonitions for mkdocs takes advantage of Admonitions of the classical material theme for mkdocs, to define extra ready-to-use admonitions usually practical in maths & general educational contexts : 

* **Maths** admonitions :
    * def / definition :gb: / définition :fr:
    * lem / lemma :gb: / lemme :fr:
    * pte / property :gb: / propriete :fr:
    * prop / proposition :gb: / proposition :fr:
    * thm / theorem :gb: / theoreme :fr:
    * python / python :gb: / python :fr:
* **General Educational** admonitions :
    * ex / exo / exercice :gb: / exercice :fr:
    * exp / example :gb: / exemple :fr:
    * mth / method :gb: / methode :fr:
    * not / notation :gb: / notation :fr:
    * rep / answer :gb: / reponse :fr:
    * cor / corr / correction :gb: / corrige :fr: / correction :fr:
    * iremember :gb: / jeretiens :fr:

**Massilia Admonitions for mkdocs are compatible with**:

* Admonitions themselves
* (Extended Massilia) Markdown tables
* Graphviz and Mermaid Diagrams

## Configuration

In the `mkdocs.yml` config file, add the following:

* In `extra_css`, add a link to rod2ik's cdn `massilia-admonitions.css`

```yaml linenums="0"
extra_css:
   # For mkdocs integration: Add a link to 'massilia-admonitions.css' cdn by rod2ik
   - https://cdn.jsdelivr.net/gh/rod2ik/cdn@main/mkdocs/css/massilia-admonitions.css
extra_javascript:
   # For mkdocs integration: Add a link to 'massilia-admonitions.js' cdn by rod2ik
  - https://cdn.jsdelivr.net/gh/rod2ik/cdn@main/mkdocs/javascripts/massilia-admonitions.js
```


That's all folks !

