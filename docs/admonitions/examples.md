# Massilia Admonitions

## A List of Massilia Maths Admonitions

### Def / Definition / Définition

=== "Rendering"
    !!! def
        a bit is `0` or `1`
=== ":fa-markdown: def"
    ```bash linenums="0"
    !!! def
        a bit is `0` or `1`
    ```
=== ":fa-markdown: definition"
    ```bash linenums="0"
    !!! definition
        a bit is `0` or `1`
    ```
=== ":fa-markdown: définition"
    ```bash linenums="0"
    !!! définition
        a bit is `0` or `1`
    ```

### Lem / Lemma / Lemme

=== "Rendering"
    !!! lem
        $e^{i\pi}=-1$
=== ":fa-markdown: lem"
    ```bash linenums="0"
    !!! lem
        $e^{i\pi}=-1$
    ```
=== ":fa-markdown: lemma"
    ```bash linenums="0"
    !!! lemma
        $e^{i\pi}=-1$
    ```
=== ":fa-markdown: lemme"
    ```bash linenums="0"
    !!! lemme
        $e^{i\pi}=-1$
    ```

### Pte / Pté / Property / Propriete / Propriété

=== "Rendering"
    !!! pte
        $e^{i\pi}=-1$
=== ":fa-markdown: pte"
    ```bash linenums="0"
    !!! pte
        $e^{i\pi}=-1$
    ```
=== ":fa-markdown: property"
    ```bash linenums="0"
    !!! property
        $e^{i\pi}=-1$
    ```
=== ":fa-markdown: propriete"
    ```bash linenums="0"
    !!! propriete
        $e^{i\pi}=-1$
    ```
=== ":fa-markdown: propriété"
    ```bash linenums="0"
    !!! propriété
        $e^{i\pi}=-1$
    ```

### Prop / Proposition / Proposition

=== "Rendering"
    !!! prop
        $e^{i\pi}=-1$
=== ":fa-markdown: prop"
    ```bash linenums="0"
    !!! prop
        $e^{i\pi}=-1$
    ```
=== ":fa-markdown: proposition"
    ```bash linenums="0"
    !!! proposition
        $e^{i\pi}=-1$
    ```

### Thm / Theorem / Théorème

=== "Rendering"
    !!! thm
        $e^{i\pi}=-1$
=== ":fa-markdown: thm"
    ```bash linenums="0"
    !!! thm
        $e^{i\pi}=-1$
    ```
=== ":fa-markdown: theorem"
    ```bash linenums="0"
    !!! theorem
        $e^{i\pi}=-1$
    ```
=== ":fa-markdown: théorème"
    ```bash linenums="0"
    !!! théorème
        $e^{i\pi}=-1$
    ```

### Proof / Preuve / Demo / Démo / Demonstration / Démonstration

=== "Rendering"
    ??? proof
        $x^2=0$ donc $x=0$
=== ":fa-markdown: proof"
    ```bash linenums="0"
    !!! proof
        $x^2=0$ donc $x=0$
    ```
=== ":fa-markdown: preuve"
    ```bash linenums="0"
    !!! preuve
        $x^2=0$ donc $x=0$
    ```
=== ":fa-markdown: démo"
    ```bash linenums="0"
    !!! démo
        $x^2=0$ donc $x=0$
    ```
=== ":fa-markdown: demo"
    ```bash linenums="0"
    !!! demo
        $x^2=0$ donc $x=0$
    ```
=== ":fa-markdown: demonstration"
    ```bash linenums="0"
    !!! demonstration
        $x^2=0$ donc $x=0$
    ```
=== ":fa-markdown: démonstration"
    ```bash linenums="0"
    !!! démonstration
        $x^2=0$ donc $x=0$
    ```

!!! preuve
    $x^2=0$ donc $x=0$

??? preuve
    $x^2=0$ donc $x=0$

### Python

!!! python
    ```python
    hey = input("your name? ")
    print(hey)
    ```

## A List of Massilia General Educational Admonitions

### Ex / Exercice / Exercice

=== "Rendering"
    !!! ex
        1. What's the weather today?
        1. What will the weather be tomorrow?
=== ":fa-markdown: ex"
    ```bash linenums="0"
    !!! ex
        1. What's the weather today?
        1. What will the weather be tomorrow?
    ```
=== ":fa-markdown: exo"
    ```bash linenums="0"
    !!! exo
        1. What's the weather today?
        1. What will the weather be tomorrow?
    ```
=== ":fa-markdown: exercice"
    ```bash linenums="0"
    !!! exercice
        1. What's the weather today?
        1. What will the weather be tomorrow?
    ```

### Exp / Examples / Exemples

=== "Rendering"
    !!! exp
        This is an example
=== ":fa-markdown: exp"
    ```bash linenums="0"
    !!! exp
        This is an example
    ```
=== ":fa-markdown: example"
    ```bash linenums="0"
    !!! example
        This is an example
    ```
=== ":fa-markdown: exemple"
    ```bash linenums="0"
    !!! exemple
        This is an example
    ```

### Mth / Method / Méthode

=== "Rendering"
    !!! mth
        1. At First, do this
        1. Second, do that
=== ":fa-markdown: mth"
    ```bash linenums="0"
    !!! mth
        1. What's the weather today?
        1. What will the weather be tomorrow?
    ```
=== ":fa-markdown: method"
    ```bash linenums="0"
    !!! method
        1. What's the weather today?
        1. What will the weather be tomorrow?
    ```
=== ":fa-markdown: méthode"
    ```bash linenums="0"
    !!! méthode
        1. What's the weather today?
        1. What will the weather be tomorrow?
    ```

### Not / Notation / Notation

=== "Rendering"
    !!! notation
        1. At First, do this
        1. Second, do that
=== ":fa-markdown: not"
    ```bash linenums="0"
    !!! not
        1. What's the weather today?
        1. What will the weather be tomorrow?
    ```
=== ":fa-markdown: notation"
    ```bash linenums="0"
    !!! notation
        1. What's the weather today?
        1. What will the weather be tomorrow?
    ```

### Rep / Reponse / Answer

=== "Rendering"
    !!! rep
        1. At First, do this
        1. Second, do that
=== ":fa-markdown: rep"
    ```bash linenums="0"
    !!! rep
        1. What's the weather today?
        1. What will the weather be tomorrow?
    ```
=== ":fa-markdown: reponse"
    ```bash linenums="0"
    !!! reponse
        1. What's the weather today?
        1. What will the weather be tomorrow?
    ```
=== ":fa-markdown: answer"
    ```bash linenums="0"
    !!! answer
        1. What's the weather today?
        1. What will the weather be tomorrow?
    ```

In your css files, set a `--correction-color` variable to customize text color in *reponse/answer* admonitions. Optionnally in dark theme too. In this example :

```yaml
# light theme:
  --correction-color: #f82e2e;
# dark theme:
  --correction-color: #ffcecc;
```

### Cor / Corr / Correction / Corrigé

=== "Rendering"
    !!! correction
        1. At First, do this
        1. Second, do that
=== ":fa-markdown: cor"
    ```bash linenums="0"
    !!! cor
        1. What's the weather today?
        1. What will the weather be tomorrow?
    ```
=== ":fa-markdown: corr"
    ```bash linenums="0"
    !!! corr
        1. What's the weather today?
        1. What will the weather be tomorrow?
    ```
=== ":fa-markdown: correction"
    ```bash linenums="0"
    !!! correction
        1. What's the weather today?
        1. What will the weather be tomorrow?
    ```
=== ":fa-markdown: corrige"
    ```bash linenums="0"
    !!! corrige
        1. What's the weather today?
        1. What will the weather be tomorrow?
    ```

In your css files, set a `--correction-color` variable to customize text color in *correction* admonitions. Optionnally in dark theme too. In this example :

```yaml
# light theme:
  --correction-color: #f82e2e;
# dark theme:
  --correction-color: #ffcecc;
```

### jeretiens / iremember

=== "Rendering"
    !!! jeretiens
        1. At First, do this
        1. Second, do that
=== ":fa-markdown: jeretiens"
    ```bash linenums="0"
    !!! jeretiens
        1. What's the weather today?
        1. What will the weather be tomorrow?
    ```
=== ":fa-markdown: iremember"
    ```bash linenums="0"
    !!! iremember
        1. What's the weather today?
        1. What will the weather be tomorrow?
    ```

### film / films / movie / movies

=== "Rendering"
    !!! movies
        1. At First, do this
        1. Second, do that
=== ":fa-markdown: film"
    ```bash linenums="0"
    !!! film
        1. What's the weather today?
        1. What will the weather be tomorrow?
    ```
=== ":fa-markdown: films"
    ```bash linenums="0"
    !!! films
        1. What's the weather today?
        1. What will the weather be tomorrow?
    ```
=== ":fa-markdown: movie"
    ```bash linenums="0"
    !!! movie
        1. What's the weather today?
        1. What will the weather be tomorrow?
    ```
=== ":fa-markdown: movies"
    ```bash linenums="0"
    !!! movies
        1. What's the weather today?
        1. What will the weather be tomorrow?
    ```

### ref / reference / references / référence / références

=== "Rendering"
    !!! références
        1. At First, do this
        1. Second, do that
=== ":fa-markdown: ref"
    ```bash linenums="0"
    !!! ref
        1. What's the weather today?
        1. What will the weather be tomorrow?
    ```
=== ":fa-markdown: réf"
    ```bash linenums="0"
    !!! réf
        1. What's the weather today?
        1. What will the weather be tomorrow?
    ```
=== ":fa-markdown: reference"
    ```bash linenums="0"
    !!! reference
        1. What's the weather today?
        1. What will the weather be tomorrow?
    ```
=== ":fa-markdown: references"
    ```bash linenums="0"
    !!! references
        1. What's the weather today?
        1. What will the weather be tomorrow?
    ```
=== ":fa-markdown: référence"
    ```bash linenums="0"
    !!! référence
        1. What's the weather today?
        1. What will the weather be tomorrow?
    ```
=== ":fa-markdown: références"
    ```bash linenums="0"
    !!! références
        1. What's the weather today?
        1. What will the weather be tomorrow?
    ```




!!! ref
    1. At First, do this
    1. Second, do that

!!! ref
    1. At First, do this
    1. Second, do that


## Per-Name Auto-numbering

Any Massilia Admonitions Type (Properties, Theorems, etc..) are auto-numbered, ... **each type being indepedant from another** (properties are counted independently from theorems, etc..)

!!! pte
    Other Property

!!! pte
    Other Property

!!! thm
    Other Theorem

!!! thm
    Other Theorem

!!! thm
    Other Theorem

!!! lem
    a Lemma

## Compatibility with Admonitions

Maths Admonitions are compatible with other Classical Admonitions from Material Theme.

!!! propriété
    === "Énoncé"
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
        nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
        massa, nec semper lorem quam in massa.
    === "Démonstration"
        Blabla $\sqrt 2$