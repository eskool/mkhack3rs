# Massilia Badges for Mkdocs

## Static Badges

### Syntax

!!! def
    By definition, <red>Static Badges</red> are **the same** in both **Light** and **Dark mode**:

    * BACKGROUND can be : 
        * either an **HTML Standard Color Name** as in [this page](https://www.w3schools.com/colors/colors_names.asp),
        * or an **HEXADECIMAL Color Code** with the `#` sign
    * BORDERS can be:
        * either the **Same Color as the Background** color ($\ssi$ NO BORDERS), **by default** (if only a background was defined)
        * or a **Static Color, Different than the the Bakcground**, if a second, optional parameter for border was given, as a `bordername` / `HEXADECIMAL` Color Code (where `bordername` is a standard HTML colorname)
    * TEXT COLOR can be:
        * Text color is a third, optional parameter that is only functional if the second optional parameter (border) was previously given
        * The **default** values for text color are
            * Black Text Color (`#000`) in Light mode,
            * White Text Color (`#fff`) in Dark mode,
        * this can be overwritten by defining a `textcolorname` / `HEXADECIMAL` Color Code (or a couple of such data, separated by a `-` sign for light and dark modes)

For **Static Badges**, the General Syntax is :

|Standard HTML Color | HEXADECIMAL Color |
|:-:|:-:|
| `<badge bakcgroundcolorname>Hey</badge>` | `<badge #XXXXXX></badge>` |
| `<bad bakcgroundcolorname>Hey</bad>` | `<bad #XXXXXX></bad>` |
| `<bd bakcgroundcolorname>Hey</bd>` | `<bd #XXXXXX></bd>` |

where:

* `colorname` is an **HTML Standard Color Name** as in [this page](https://www.w3schools.com/colors/colors_names.asp)
* an $6$ digit (or $8$-digit) HEXADECIMAL Color Code, as in CSS, WITH the # sign : 
    * $6$ digits : `#XXXXXX` NO OPACITY
    * $8$ digits : `#XXXXXXYY` WITH OPACITY

### Examples

| Rendering | Allowed Syntaxes |
|:-:|:-:|
| <bd aquamarine>Aquamarine</bd> | `<badge aquamarine></badge>`<br/>`<bad aquamarine></bad>`<br/>`<bd aquamarine></bd>` |
| <bd blueviolet>BlueViolet</bd> | `<badge blueviolet></badge>`<br/>`<bad blueviolet></bad>`<br/>`<bd blueviolet></bd>` |
| <bd chartreuse>Chartreuse</bd> | `<badge chartreuse></badge>`<br/>`<bad chartreuse></bad>`<br/>`<bd chartreuse></bd>` |
| <bd crimson>Crimson</bd> | `<badge deepskyblue></badge>`<br/>`<bad deepskyblue></bad>`<br/>`<bd deepskyblue></bd>` |
| <bd deepskyblue>DeepSkyBlue</bd> | `<badge deepskyblue></badge>`<br/>`<bad deepskyblue></bad>`<br/>`<bd deepskyblue></bd>` |
| ... | ... |


<badge #ff0000>Hello</badge>

## Dynamic Badges

The **default** badge is a Dynamic Badge.

### Syntax

|<bd>DEFAULT BADGE</bd>| `<badge>DEFAULT BADGE</baged>`<br/>`<bad>DEFAULT BADGE</bad>`<br/>`<bd>DEFAULT BADGE</bd>`<br/> |

### Examples

| Rendering | Syntaxes |
|:-:|:-:|
| <bagrey>grey</bagrey> |  |
| <bapink>pink</bapink> |  |
| <bared>red</bared> |  |
| <bascarlet>scarlet</bascarlet> |  |
| <bayellow>yellow</bayellow> | |
| <bagreen>green</bagreen> |  |
| <bableu>blue</bableu> |  |

