# Massilia Badges for Mkdocs

## Introduction

<bd>Badges</bd> are smalls colored pills surrounding markdown text, to be defined by one of the following HTML tags : `badge` / `bad` / `bd`, that have:

* a <badge>Background</badge> color. **Default behavior** is <bd @gris>hey</bd>
    * a custom dynamic grey called `gris` - to stick to international HTML Standard color names
* a <bd>Border</bd> color (optional), bottom and right presently. **Default behavior** is :
    * <bd gris>NO Borders</bd> for **static badges**,
    * <bd @gris>Specific & Customisable Borders</bd> for **dynamic badges**
    * and for dynamic badges)
* a <bd>Text</bd> color (optional). **Default behavior** is :
    * `#000` in Light mode (mkdocs light theme), and 
    * `#fff` in Dark mode (mkdocs dark theme)

We'll be using the following definitions. Very Generally :

* a **Static Badge** (as for *background*, *border* or *text* colors are concerned) if these parameters are **the same** in both <bd @grey>Light mode</bd> and <bd @grey>Dark mode</bd>
* a **Dynamic Badge** has its colors which are **NOT the same** in <bd @grey>Light mode</bd> and <bd @grey>Dark mode</bd>.

## Configuration

## Configuration

### Standard Configuration

In the `mkdocs.yml` config file, add the following two lines:

* In `extra_css`, add a link to rod2ik's cdn `massilia-badges.css`
* In `extra_javascript`, add a link to rod2ik's cdn `massilia-badges.js`

```yaml linenums="0"
extra_css:
   # For mkdocs integration: Add a link to 'massilia-badges.css' cdn by rod2ik
   - https://cdn.jsdelivr.net/gh/rod2ik/cdn@main/mkdocs/css/massilia-badges.css
extra_javascript:
   # For mkdocs integration: Add a link to 'massilia-badges.js' cdn by rod2ik
  - https://cdn.jsdelivr.net/gh/rod2ik/cdn@main/mkdocs/javascripts/massilia-images.js
```

That's all folks ! at least if you just want to use standard (static and dynamic) massilia badges.

### Advanced & Customisable Configuration

#### Configuration

IF you want to **customise your own colors**, and even overwrite HTML Standard color names, **you can**, provided you add the following lines in your `overrides/main.html` file :

```bash linenums="0"
{{ '{%' }} block content %}
    {{ '{{' }} super() }}
    <massilia data="{{ '{{' }} config.extra.massilia }}"></massilia>
{{ '{%' }} endblock %}
```

If you don't already have such a `overrides/main.html` page, create one.



