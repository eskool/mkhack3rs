# Mermaid CSS for mkdocs

## What is Mermaid CSS for mkdocs ?

Mermaid diagrams in mkdocs is a functionnality already developped by the `pymdownx.superfences extension` (needed). However, in my humble opinion, mermaid diagrams in mkdocs need a bit more of styling via CSS (and JS) to be natively good looking, espacially with Light and Dark themes of mkdocs.

The purpose of the following cdn links (CSS and JS files) is :

* to save you some precious time, and 
* to offer you a one-line-install (two-lines..) procedure (via rod2ik's cdn) to render mermaid diagrams in mkdocs in a way as close as possible to the beautiful site [mermaid.live](https://mermaid.live)

Mermaid Diagrams:

* defaults' are meant to be natively used *as is*
* are natively adapted to both Light & Dark Themes within mkdocs
* are highly customisable, via CSS variables, if you decide to download the files for further customisation (Copyleft)

## Configuration

In the `mkdocs.yml` config file, add the following:

* In `extra_css`, add a link to rod2ik's cdn `massilia-mermaid.css`
* In `extra_javascript` :
    * add a link to an official mermaid cdn, e.g. :  
    https://cdnjs.cloudflare.com/ajax/libs/mermaid/8.13.8/mermaid.min.js
    * (then) add a link to rod2ik's cdn `massilia-mermaid.js`

```yaml linenums="0"
markdown_extensions:
  - pymdownx.superfences:
      custom_fences:
        - name: mermaid
          class: mermaid
          format: !!python/name:pymdownx.superfences.fence_div_format
extra_css:
   # For mkdocs integration: Add a link to 'massilia-mermaid.css' cdn by rod2ik
   - https://cdn.jsdelivr.net/gh/rod2ik/cdn@main/mkdocs/css/massilia-mermaid.css

extra_javascript:
  # Add a link to the official mermaid CDN
  - https://cdnjs.cloudflare.com/ajax/libs/mermaid/8.13.8/mermaid.min.js
  # For mkdocs integration: Add a link to 'massilia-mermaid.js' cdn by rod2ik
  - https://cdn.jsdelivr.net/gh/rod2ik/cdn@main/mkdocs/javascripts/massilia-mermaid.js
```

That's all folks !
