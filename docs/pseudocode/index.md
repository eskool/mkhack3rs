# Pseudocode

## What is PseudocodeFr ?

`PseudocodeFr` is an mkdocs extension to style pseudocode in several Languages (currently just french).
This page describes Pseudocode for the French language (PseudocodeFr).

## Configuration

The lexer is available as a Pip package:

```bash
pip install pygments-lexer-pseudocode-fr
```

