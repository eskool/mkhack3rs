# PseudoCodeFr Examples

## With `pseudo` or `pseudocode` code block

### Example 1

=== "Rendering"
    ```pseudo
    Saisir cible
    Trouve = FAUX
    Pour chaque élément el du tableau A
    Si el = cible
    Alors Trouve = VRAI
    Afficher Trouve
    ```

    with greek letters and caracter pairing:

    ```pseudo
    Saisir \alpha, \beta
    Si \alpha >= 0:
    Alors:
        afficher 2*\alpha >= 0
    Sinon:
        afficher 2*\alpha <0
    Si \beta != 0:
    Alors afficher \beta
    ```
=== ":fa-markdown: Markdown with `pseudo`"
    ````markdown
        ```pseudo
        Saisir cible
        Trouve = FAUX
        Pour chaque élément el du tableau A
        Si el = cible
        Alors Trouve = VRAI
        Afficher Trouve
        ```
    ````

    with greek letters (LaTeX style) and caracter pairing (operations `>=`, `<=`, `!=`, arrows `->`, `<-`):

    ````markdown
        ``` pseudo
        Saisir \alpha, \beta
        Si \alpha >= 0:
        Alors:
            afficher 2*\alpha >= 0
        Sinon:
            afficher 2*\alpha <0
        Si \beta != 0:
        Alors afficher \beta
        ```
    ````
=== ":fa-markdown: Markdown with `pseudocode`"
    ````markdown
        ```pseudocode
        Saisir cible
        Trouve = FAUX
        Pour chaque élément el du tableau A
        Si el = cible
        Alors Trouve = VRAI
        Afficher Trouve
        ```
    ````

    with greek letters (LaTeX style) and caracter pairing (operations `>=`, `<=`, `!=`, arrows `->`, `<-`):

    ````markdown
        ``` pseudocode
        Saisir \alpha, \beta
        Si \alpha >= 0:
        Alors:
            afficher 2*\alpha >= 0
        Sinon:
            afficher 2*\alpha <0
        Si \beta != 0:
        Alors afficher \beta
        ```
    ````

### Example 2

=== "Rendering"
    ```pseudo
    # Recherche Dichotomique de l'occurence d'une valeur C dans un tableau A TRIÉ
    fonction recherche_dico(C,A):
    n prend la valeur taille(A)
    gauche prend la valeur 0
    droite prend la valeur n - 1
    Tant que gauche <= droite:
        milieu prend la valeur partie entière de [(Gauche + Droite) / 2]
        Si C > A[milieu] Alors:
        gauche = milieu + 1
        Sinon Si C < A[milieu] Alors:
        droite = milieu - 1
        Sinon:
            retourner milieu
    retourner Vide
    ```
=== ":fa-markdown: Markdown with `pseudo`"
    ````markdown
        ```pseudo
        # Recherche Dichotomique de l'occurence d'une valeur C dans un tableau A TRIÉ
        fonction recherche_dico(C,A):
        n prend la valeur taille(A)
        gauche prend la valeur 0
        droite prend la valeur n - 1
        Tant que gauche <= droite:
            milieu prend la valeur partie entière de [(Gauche + Droite) / 2]
            Si C > A[milieu] Alors:
            gauche = milieu + 1
            Sinon Si C < A[milieu] Alors:
            droite = milieu - 1
            Sinon:
                retourner milieu
        retourner Vide
        ```
    ````
=== ":fa-markdown: Markdown with `pseudocode`"
    ````markdown
        ```pseudocode
        # Recherche Dichotomique de l'occurence d'une valeur C dans un tableau A TRIÉ
        fonction recherche_dico(C,A):
        n prend la valeur taille(A)
        gauche prend la valeur 0
        droite prend la valeur n - 1
        Tant que gauche <= droite:
            milieu prend la valeur partie entière de [(Gauche + Droite) / 2]
            Si C > A[milieu] Alors:
            gauche = milieu + 1
            Sinon Si C < A[milieu] Alors:
            droite = milieu - 1
            Sinon:
                retourner milieu
        retourner Vide
        ```
    ````

## List of words

Please consider having a look to Project page : [:fa-gitlab: rod2ik/pygments-lexer-pseudocode-fr](https://gitlab.com/rod2ik/pygments-lexer-pseudocode-fr)