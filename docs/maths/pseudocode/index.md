# Mkhack3rs - Pygments Lexer for Pseudocode Language in French

```pseudo
SI A est VRAI ALORS
    A devient FAUX
variable globale compte                      /* commun */
FONCTION entrer_SC_compte():
    ...
FONCTION sortir_SC_compte():
    ...
entier A
AFFICHER("ajout de 1000€")                  /* tâche 1 */
SAISIR("BONJOUR")
POUR i VARIANT DE 1 À 5:
    FAIRE i = i+1
    j = 2*3+4^2
    \alpha\beta\gamma\delta\epsilon\zeta\eta\theta\iota\kappa\lambda\mu\nu\xi\pi\rho\sigma\tau\upsilon\phi\chi\psi\omega
    \Alpha\Beta\Gamma\Delta\Epsilon\Zeta\Eta\Theta\Iota\Kappa\Lambda\Mu\Nu\Xi\Pi\Rho\Sigma\Tau\Upsilon\Phi\Chi\Psi\Omega
    

SI A>=5 ET A=5 OU (A+2<10)
ALORS AFFICHER("Bonjour")
SI (age != 18 ET age <= 25) OU age >= 30
ALORS afficher("T trop jeune")
```
