# mkdocs-asy Examples

## Example 1

=== "Rendering"
    ```asy
    import math;

    size(6cm,0);

    add(grid(6,5,lightgray));

    pair pA=(1,1), pB=(5,1), pC=(4,4);
    
    draw(pA--pB--pC--cycle);
    dot("$A$",pA,dir(pC--pA,pB--pA));
    dot("$B$",pB,dir(pC--pB,pA--pB));
    dot("$C$",pC,dir(pA--pC,pB--pC));
    ```
=== "Markdown"
    ```bash linenums="0"
     ``` asy
     import math;
     size(6cm,0);
     add(grid(6,5,lightgray));
     pair pA=(1,1), pB=(5,1), pC=(4,4);
     draw(pA--pB--pC--cycle);  
     dot("$A$",pA,dir(pC--pA,pB--pA));  
     dot("$B$",pB,dir(pC--pB,pA--pB));  
     dot("$C$",pC,dir(pA--pC,pB--pC));  
     ```
    ```

Note there must be NO spaces between the 3 bacticks` ``` ` and the keyword `asy`

## Example 2

=== "Rendering"
    ```asy myfile.svg
    import math;
    import markers;

    size(6cm, 0);

    // Définition de styles de points
    marker croix = marker(scale(3)*cross(4),
                        1bp+gray);

    add(grid(6, 6, .8lightgray));

    pair pA = (1, 1), pB = (5, 5), pC = (2, 4);

    draw(pA--pB--pC--cycle);
    draw("$A$", pA, SW,blue, croix);  
    draw("$B$", pB, SE,blue, croix);  
    draw("$C$", pC, NW, croix);

    draw(pA--pC, StickIntervalMarker(1, 2, size=8, angle=-45, red));
    draw(pB--pC, StickIntervalMarker(1, 2, size=8, angle=-45, red));
    ```
=== "Markdown"
    ```bash linenums="0"
     ``` asy myfile.svg
     import math;
     import markers;
     size(6cm, 0);
     // Définition de styles de points
     marker croix = marker(scale(3)*cross(4),
                         1bp+gray);
     add(grid(6, 6, .8lightgray));
     pair pA = (1, 1), pB = (5, 5), pC = (2, 4);
     draw(pA--pB--pC--cycle);
     draw("$A$", pA, SW,blue, croix);
     draw("$B$", pB, SE,blue, croix);
     draw("$C$", pC, NW, croix);
     draw(pA--pC, StickIntervalMarker(1, 2, size=8, angle=-45, red));
     draw(pB--pC, StickIntervalMarker(1, 2, size=8, angle=-45, red));
     ```
    ```

Note there must be NO spaces between the 3 bacticks` ``` ` and the keyword `asy`

## Compatiblity with Admonitions

???+ note "Open Block with toto"
    ```asy
    import math;

    size(6cm,0);

    add(grid(6,5,lightgray));

    pair pA=(1,1), pB=(5,1), pC=(4,4);

    draw(pA--pB--pC--cycle);
    dot("$A$",pA,dir(pC--pA,pB--pA));
    dot("$B$",pB,dir(pC--pB,pA--pB));
    dot("$C$",pC,dir(pA--pC,pB--pC));
    ```