# Examples of Maths Variation Tables

:warning: NO SPACE BETWEEN THE THREE BACKTICKS AND THE `vartable` keyword. :warning:

## No Color `tkz-tab` Syntax

=== "Rendering"
    ```vartable
    \begin{tikzpicture}
        \tkzTabInit[espcl=6]
            {$x$/1 , $f'(x)$/1 , $f(x)$/2}
            {$0$ , $\sqrt{e}$ , $+\infty$}
        \tkzTabLine{d,+,0,+,}
        \tkzTabVar{D- / $-\infty$ , R/ , + / $0$ }
        \tkzTabVal[draw]{1}{3}{0.4}{$1$}{$-e$}
    \end{tikzpicture}
    ```
=== "Code :fa-markdown:"
    ```markdown
        ``` vartable
        \begin{tikzpicture}
            \tkzTabInit[espcl=6]
                {$x$/1 , $f'(x)$/1 , $f(x)$/2}
                {$0$ , $\sqrt{e}$ , $+\infty$}
            \tkzTabLine{d,+,0,+,}
            \tkzTabVar{D- / $-\infty$ , R/ , + / $0$ }
            \tkzTabVal[draw]{1}{3}{0.4}{$1$}{$-e$}
        \end{tikzpicture}
        ```
    ```

### Example 2

=== "Rendering"
    ```vartable
    \begin{tikzpicture}
    \newcommand*{ \E}{ \ensuremath{ \mathrm{e}}}.
    \tkzTabInit{$x$ /1,$g'(x)$ /1,$g(x)$ /2}
    {$-\infty$,$-2$,$0$,$+\infty$}
    \tkzTabLine{,+,z,-,z,-}
    \tkzTabVar{-/$-\infty$, +/$2$, R/, -/$-\infty$}
    \tkzTabIma{2}{4}{3}{$0$}
    \end{tikzpicture}
    ```
=== "Code :fa-markdown:"
    ```markdown
        ``` vartable
        \begin{tikzpicture}
        \newcommand*{ \E}{ \ensuremath{ \mathrm{e}}}.
        \tkzTabInit{$x$ /1,$g'(x)$ /1,$g(x)$ /2}
        {$-\infty$,$-2$,$0$,$+\infty$}
        \tkzTabLine{,+,z,-,z,-}
        \tkzTabVar{-/$-\infty$, +/$2$, R/, -/$-\infty$}
        \tkzTabIma{2}{4}{3}{$0$}
        \end{tikzpicture}
        ```
    ```

### Example 3

=== "Rendering"
    ```vartable
    \begin{tikzpicture}
    \tkzTabInit[lgt=3,espcl=1.5]
        {$x$ / 1 , $f'(x)$ / 1, $f(x)=\sqrt{\dfrac{x-1}{x+1}}$ / 2 }
        {$-\infty$, $-1$, $1$, $+\infty$}
    \tkzTabLine
        { , + , d , h , d, +, }
    \tkzTabVar
        { -/1 , +DH/$+\infty$, -C/0, +/1}
    \end{tikzpicture}
    ```
=== "Code :fa-markdown:"
    ```markdown
        ``` vartable
        \begin{tikzpicture}
        \tkzTabInit[lgt=3,espcl=1.5]
            {$x$ / 1 , $f'(x)$ / 1, $f(x)=\sqrt{\dfrac{x-1}{x+1}}$ / 2 }
            {$-\infty$, $-1$, $1$, $+\infty$}
        \tkzTabLine
            { , + , d , h , d, +, }
        \tkzTabVar
            { -/1 , +DH/$+\infty$, -C/0, +/1}
        \end{tikzpicture}
        ```
    ```

### Example 4

=== "Rendering"
    ```vartable
    \begin{tikzpicture}
    \tkzTabInit[lgt=2.75]
        { $x$/1  ,  $f'(x)$/1  ,  $f(x) = tan(x)$/2  }
        { $0$,  $\dfrac{\pi}{2}$,  $\pi$}
    \tkzTabLine
        { , + , d , + , }
    \tkzTabVar
        { -/$0$ ,  +D-/$+\infty$/$-\infty$ , +/$0$}
    \end{tikzpicture}
    ```
=== "Code :fa-markdown:"
    ```markdown
        ``` vartable
        \begin{tikzpicture}
        \tkzTabInit[lgt=2.75]
            { $x$/1  ,  $f'(x)$/1  ,  $f(x) = tan(x)$/2  }
            { $0$,  $\dfrac{\pi}{2}$,  $\pi$}
        \tkzTabLine
            { , + , d , + , }
        \tkzTabVar
            { -/$0$ ,  +D-/$+\infty$/$-\infty$ , +/$0$}
        \end{tikzpicture}
        ```
    ```

## Coloring `tkz-tab` Syntax

**mkdocs-tex2svg** is compatible with `tkz-tab` colouring syntax.

### Example 5

=== "Rendering"
    ```vartable
    \begin{tikzpicture}
    \tikzset{h style/.style = {fill=red!50}}
    \tkzTabInit[lgt=1,espcl=2]{$x$ /1, $f$ /2}{$0$,$1$,$2$,$3$}%
    \tkzTabVar{+/ $1$ / , -CH/ $-2$ / , +C/ $5$, -/ $0$ / }
    \end{tikzpicture}
    ```
=== "Code :fa-markdown:"
    ```markdown
        ``` vartable
        \begin{tikzpicture}
        \tikzset{h style/.style = {fill=red!50}}
        \tkzTabInit[lgt=1,espcl=2]{$x$ /1, $f$ /2}{$0$,$1$,$2$,$3$}%
        \tkzTabVar{+/ $1$ / , -CH/ $-2$ / , +C/ $5$, -/ $0$ / }
        \end{tikzpicture}
        ```
    ```

### Exercice 6

=== "Rendering"
    ```vartable
    \begin{tikzpicture}
    \tkzTabInit[color,
    colorT = yellow!20,
    colorC = red!20,
    colorL = green!20,
    colorV = lightgray!20,
    lgt = 1,
    espcl = 2.5]%
    {$t$/1,$a$/1,$b$/1,$c$/1,$d$/1}%
    {$\alpha$,$\beta$,$\gamma$}%
    \end{tikzpicture}
    ```
=== "Code :fa-markdown:"
    ```markdown
        ``` vartable
        \begin{tikzpicture}
        \tkzTabInit[color,
        colorT = yellow!20,
        colorC = red!20,
        colorL = green!20,
        colorV = lightgray!20,
        lgt = 1,
        espcl = 2.5]%
        {$t$/1,$a$/1,$b$/1,$c$/1,$d$/1}%
        {$\alpha$,$\beta$,$\gamma$}%
        \end{tikzpicture}
        ```
    ```

## Admonitions

**mkdocs-tex2svg** is compatible with mkdocs Admonitions AND Massilia (mainly Maths) Admonitions.

### Exemple 7

=== "Rendering"
    !!! ex
        ```vartable
        \begin{tikzpicture}
            \tkzTabInit[lgt=3, espcl=6, deltacl=0.7]{$x$ /1, $(e^x)'=e^x$ /1, $e^x$ /1.5} {$-\infty$ , $+\infty$}
        \tkzTabLine{, -,}
        \tkzTabVar{-/ , +/ }
        \tkzTabVal{1}{2}{0.3}{$0$}{1}
        \tkzTabVal{1}{2}{0.6}{$1$}{$e$}
        \end{tikzpicture}
        ```
=== "Code :fa-markdown:"
    ```markdown
        !!! ex
            ``` vartable
            \begin{tikzpicture}
                \tkzTabInit[lgt=3, espcl=6, deltacl=0.7]{$x$ /1, $(e^x)'=e^x$ /1, $e^x$ /1.5} {$-\infty$ , $+\infty$}
            \tkzTabLine{, +,}
            \tkzTabVar{-/ , +/ }
            \tkzTabVal{1}{2}{0.3}{$0$}{1}
            \tkzTabVal{1}{2}{0.6}{$1$}{$e$}
            \end{tikzpicture}
            ```
    ```

### Exemple 8

=== "Rendering"
    ??? ex
        ```vartable
        \begin{tikzpicture}
            \tkzTabInit[lgt=3, espcl=6, deltacl=0.7]{$x$ /1, $(e^x)'=e^x$ /1, $e^x$ /1.5} {$-\infty$ , $+\infty$}
        \tkzTabLine{, +,}
        \tkzTabVar{-/ , +/ }
        \tkzTabVal{1}{2}{0.3}{$0$}{1}
        \tkzTabVal{1}{2}{0.6}{$1$}{$e$}
        \end{tikzpicture}
        ```
=== "Code :fa-markdown:"
    ```markdown
        ??? ex
            ``` vartable
            \begin{tikzpicture}
                \tkzTabInit[lgt=3, espcl=6, deltacl=0.7]{$x$ /1, $(e^x)'=e^x$ /1, $e^x$ /1.5} {$-\infty$ , $+\infty$}
            \tkzTabLine{, +,}
            \tkzTabVar{-/ , +/ }
            \tkzTabVal{1}{2}{0.3}{$0$}{1}
            \tkzTabVal{1}{2}{0.6}{$1$}{$e$}
            \end{tikzpicture}
            ```
    ```
