# Maths Variation Tables

Project Page : :fa-gitlab: [mkdocs-tex2svg](https://gitlab.com/rod2ik/mkdocs-tex2svg)

## Installation of the mkdocs-tex2svg Extension

Note that a comprehensive LaTeX distribution must be installed on the server (or locally for debugging) for this extension to work with mkdocs, and hence draw maths variation tables.

`$ sudo pacman -S texlive-meta`  
`$ sudo pacman -S pdf2svg`  
`$ pip install mkdocs-tex2svg`  

## Configuration

```yaml
markdown_extensions:
  - mkdocs_tex2svg
```

Add these CSS snippets to your custom CSS:

In your **Light Theme Css** :

```css
  --vartab-light-display: block;
  --vartab-dark-display: none;

.md-typeset .light {
  display: var(--vartab-light-display);
}

.md-typeset .dark {
  display: var(--vartab-dark-display);
}
```

in your **Dark Theme Css** :

```css
--vartab-light-display: none;
--vartab-dark-display: block;
```

That's All Folks :)

## Config Options

**Optionnally**, use any (or a combination) of the following options with all colors being written as:

* a **standard HTML Color Name** as in [this W3C page](https://www.w3schools.com/tags/ref_colornames.asp) (All Caps Allowed)
* an **HTML HEXADECIMAL COLOR, but WITHOUT THE # SIGN**

```yaml
markdown_extensions:
  - mkdocs_tex2svg:
      vartable:                     # Specific Configs for Variations Tables
        light:                      # Light Theme Configs
          color: 044389                 # Any HTML Color Name, or, any HTML Hexadecimal color code WITHOUT the `#` sign
          bglabel: darksalmon           # Any HTML Color Name, or, any HTML Hexadecimal color code WITHOUT the `#` sign
          bgvi: FFFFFF                  # Any HTML Color Name, or, any HTML Hexadecimal color code WITHOUT the `#` sign
        dark:                       # Dark Theme Configs
          color: lavenderblush          # Any HTML Color Name, or, any HTML Hexadecimal color code WITHOUT the `#` sign
          bglabel: 7F0385               # Any HTML Color Name, or, any HTML Hexadecimal color code WITHOUT the `#` sign
          bgvi: red                     # Any HTML Color Name, or, any HTML Hexadecimal color code WITHOUT the `#` sign
        priority: 75                 # The priority for this Markdown Extension (DEFAULT : 75)
```

Where:

* `vartable` refers to configs which are specific to Maths Variation Tables
* `light` is the keyword for configs relative to **Light Theme** in Mkdocs
* `dark` is the keyword for configs relative to **Dark Theme** in Mkdocs
* `color` is a color option that modifies **The Color** of Variation Tables :
    * Borders and Arrows
    * All Texts (Labels, annd non Labels)
    * **Defaults** for **colors** are :
        * `black` for Light Theme, and 
        * `white` for Dark Theme
* `bglabel` sets the Bakcground Color the the **Labels** (texts upon arrows mainly). 
    * **Defaults** for **bgLabels** are:
        * `white` for Light Theme, and 
        * `2E303E` for Dark Theme = Default Mkdocs Material Dark Background for Slate
* `bgvi` sets the Background Color for **Valeurs Interdites (VI)** / **Forbidden Values (FV)** (the background inside the double vertical bars)
    * **Defaults** for **bgvi** are :
        * `white` for Light Theme, and 
        * `2E303E` for Dark Theme = Default Mkdocs Material Dark Background for Slate
* `priority` (default `75`) sets the priority for this Markdown Extension

## Color Codes

Color Codes can be :

* a **standard HTML Color Name** as in [this W3C page](https://www.w3schools.com/tags/ref_colornames.asp) (All Caps Allowed)
* an **HTML HEXADECIMAL COLOR, WITHOUT THE # SIGN**

## Mixing & Conflicting Options

* No known conflicts with the **tkz-tab** syntax (may be the bglabels? feel free feedback any issue)

## Usage

To use it in your Markdown doc, 

with SVG output (no space between the three backticks and `vartable`)

```html
  {% raw %}
    ``` vartable
    \begin{tikzpicture}
    \tikzset{h style/.style = {fill=red!50}}
    \tkzTabInit[lgt=1,espcl=2]{$x$ /1, $f$ /2}{$0$,$1$,$2$,$3$}%
    \tkzTabVar{+/ $1$ / , -CH/ $-2$ / , +C/ $5$, -/ $0$ / }
    \end{tikzpicture}
    ```
  {% endraw %}
```

## Examples

Other examples in these pages:

* Installation & Configs : [https://eskool.gitlab.io/mkhack3rs/maths/tables/](https://eskool.gitlab.io/mkhack3rs/maths/tables/)
* Examples : [https://eskool.gitlab.io/mkhack3rs/maths/tables/examples/](https://eskool.gitlab.io/mkhack3rs/maths/tables/examples/)

## CSS / JS Classes

* Each `vartable` svg img is preceded by a span tag with the two classes : `tex2svg` and `vartable.

## Credits

* Rodrigo Schwencke for all credits : [rod2ik/mkdocs-tex2svg](https://gitlab.com/rod2ik/mkdocs-tex2svg)

## License

* All parts are from [Rodrigo Schwencke / rod2ik](https://gitlab.com/rod2ik) are [GPLv3+](https://opensource.org/licenses/GPL-3.0)
