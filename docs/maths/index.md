# Typing Maths

Inline : $\sqrt 2$

et

$$\sum_{n=1}^{+\infty} \frac 1{n^2} = \frac {\pi^2}{6}$$

## MathJaX Configs

=== "Rendering"
    $
    \begin{pmatrix*}[r]
    -1 & 3 \\
    2 & -4
    \end{pmatrix*}
    \m N
    \m Z
    \m Q
    \m R
    \m C
    \ssi
    \sssi
    \imp
    \iimp
    $

    $
    \unicode{67}
    \cal(Allo)
    \vec x
    \Vert {a} \Vert
    \text {allo}
    A \xLeftarrow[under]{over} B
    \Braket{ \phi | \frac{\partial^2}{\partial t^2} | \psi }
    \abs {2}
    \grad
    \bbox[5px, border: 2px solid red]{allo}
    $

    $
        f(x)=\cancel{5} +6x+9-x^2=x^2+4x+1
    $

    \begin{equation}
    x^2=1 \label{eq:hey}
    \end{equation}

    donc d'après $\eqref{eq:hey}$

    $
        \beth
        \delta
        \Delta  
        verbatim : \, \verb|\alpha|  
        \det(A)
        \Re(z)
    f(x)=\cancel{5} +6x+9-x^2=x^2+4x+1
        \bbox[red]{x+y}
    $

    \begin{empheq}[left = \empheqlbrace]{align}
        eq1 && conditions1\\
        eq2 && conditions2\\
        eq3 && conditions3
    \end{empheq}

    \begin{numcases} {|x|=}
    x, & for $x \geq 0$\\
    -x, & for $x < 0$
    \end{numcases}

    \begin{array}{|l|c|}
    \rowcolor[gray]{.5}\columncolor{red} one & two\\
    \rowcolor{lightblue} three & four\\\hline
    five & six \\
    \rowcolor{magenta}seven & \cellcolor{green}eight
    \end{array}

    bbox: $\bbox[blue, 5px, border: 2px solid antiquewhite]{allo3}$
    bbox: $\bbox[5px, border: 2px solid antiquewhite]{allo3}$

    box: $\box{red}{Allo}$  
    box: $\box{green}{Allo}$  
    box: $\box{aquamarine}{Allo}$  
    redbox: $\redbox{allo1}$  
    greenbox: $\greenbox{allo1}$  
    bluebox: $\bluebox{allo1}$  

    textcolor: $\textcolor{aquamarine}{texteEnCouleur}$
    color: $\color{red}{color}$  
    color: $\color{}{color}$  
    color: $\color{red}{x} + \color{blue}{y}$
    color: ${\color{red} x} + {\color{blue} y}$

    colorbox: $ \colorbox{red}{fcolorbox}$  

    fcolorbox : $\fcolorbox{lawngreen}{red}{custom color WITH background}$  
    fcolorbox : $\fcolorbox{lawngreen}{}{custom color with NO background}$  
    fcolorbox : $\fcolorbox{black}{gray!30}{My text}$  
    enclose: $\enclose{circle}[mathcolor="red"]{\color{black}{x}}$
=== ":fa-markdown: LaTeX"
    ```latex
    $
    \begin{pmatrix*}[r]
    -1 & 3 \\
    2 & -4
    \end{pmatrix*}
    \m N
    \m Z
    \m Q
    \m R
    \m C
    \ssi
    \sssi
    \imp
    \iimp
    $

    $
    \unicode{67}
    \cal(Allo)
    \vec x
    \Vert {a} \Vert
    \text {allo}
    A \xLeftarrow[under]{over} B
    \Braket{ \phi | \frac{\partial^2}{\partial t^2} | \psi }
    \abs {2}
    \grad
    \bbox[5px, border: 2px solid red]{allo}
    $

    $
        f(x)=\cancel{5} +6x+9-x^2=x^2+4x+1
    $

    \begin{equation}
    x^2=1 \label{eq:hey}
    \end{equation}

    donc d'après $\eqref{eq:hey}$

    $
        \beth
        \delta
        \Delta  
        verbatim : \, \verb|\alpha|  
        \det(A)
        \Re(z)
    f(x)=\cancel{5} +6x+9-x^2=x^2+4x+1
        \bbox[red]{x+y}
    $

    \begin{empheq}[left = \empheqlbrace]{align}
        eq1 && conditions1\\
        eq2 && conditions2\\
        eq3 && conditions3
    \end{empheq}

    \begin{numcases} {|x|=}
    x, & for $x \geq 0$\\
    -x, & for $x < 0$
    \end{numcases}

    \begin{array}{|l|c|}
    \rowcolor[gray]{.5}\columncolor{red} one & two\\
    \rowcolor{lightblue} three & four\\\hline
    five & six \\
    \rowcolor{magenta}seven & \cellcolor{green}eight
    \end{array}

    bbox: $\bbox[blue, 5px, border: 2px solid antiquewhite]{allo3}$
    bbox: $\bbox[5px, border: 2px solid antiquewhite]{allo3}$

    box: $\box{red}{Allo}$  
    box: $\box{green}{Allo}$  
    box: $\box{aquamarine}{Allo}$  
    redbox: $\redbox{allo1}$  
    greenbox: $\greenbox{allo1}$  
    bluebox: $\bluebox{allo1}$  

    textcolor: $\textcolor{aquamarine}{texteEnCouleur}$
    color: $\color{red}{color}$  
    color: $\color{}{color}$  
    color: $\color{red}{x} + \color{blue}{y}$
    color: ${\color{red} x} + {\color{blue} y}$

    colorbox: $ \colorbox{red}{fcolorbox}$  

    fcolorbox : $\fcolorbox{lawngreen}{red}{custom color WITH background}$  
    fcolorbox : $\fcolorbox{lawngreen}{}{custom color with NO background}$  
    fcolorbox : $\fcolorbox{black}{gray!30}{My text}$  
    enclose: $\enclose{circle}[mathcolor="red"]{\color{black}{x}}$
    ```
