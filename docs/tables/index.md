# XTables : Extended Markdown Tables

## What are Extended Tables for mkdocs?

They extend the normal syntax of tables in markdown syntax used in mkdocs, and allow several cells to be merged.

## Configuration

In the `overrides/main.html` file, add the following lines, so that the file looks something like this :

```html
{% raw %}
{% extends "base.html" %}

{% block htmltitle %}
{{ super() }}
<title>{{ base_url }}</title>
{% endblock %}

{% block content %}
    {{ super() }}
    <massilia data="{{ config.extra.massilia }}"></massilia>
{% endblock %}

{% block libs %}
    {{ super() }}
    <!-- Massilia javascripts -->
    <script src="{{ base_url }}/assets/javascripts/massilia.js" defer></script>
{% endblock %}
{% endraw %}
```

That's all folks !

