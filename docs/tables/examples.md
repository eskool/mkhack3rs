# Massilia XTables (Extended Tables)

## Add Classes, IDs & Attributes to Tables

### Use `{ }` or `{: :}` Syntax

Add `{   }` or `{:   :}` in the first line following a table in makdown syntax, with some information inside :

* Some **Classes** `{ .class1 .class2 }` or `{: .class1 .class2 :}`
* Some **ID**(s) `{ #myid }` or `{: #myid :}`
* Some **Attributes** `{ attr1="hello" attr2="world" }` or `{: attr1="hello" attr2="world" :}`

Both `{   }` and `{:   :}` syntaxes are allowed for a table given in markdown format.

### Adding Classes to Tables

For example, to add the two classes `{.allo .quoi}` to the `table` 

=== "Rendering"
    |A|B|C|
    |:-:|:-:|:-:|
    | a | b | $\sqrt 2$ |
    | 1 | 2 | 3 |
    { .allo .quoi }
=== "Markdown :fa-markdown:"
    ```markdown
    |A|B|C|
    |:-:|:-:|:-:|
    | a | b | $\sqrt 2$ |
    | 1 | 2 | 3 |
    { .allo .quoi }
    ```
=== "HTML Created :fa-html5:"
    adds classes `.allo` and `.quoi` to the rendered HTML `table` element :  

    ```html
    <table class="allo quoi">
    <thead>
    <tr>
    <th align="center">A</th>
    <th align="center">B</th>
    <th align="center">C</th>
    </tr>
    </thead>
    <tbody>
    <tr>
    <td align="center">a</td>
    <td align="center">b</td>
    <td align="center"><span class="arithmatex"><mjx-container class="MathJax CtxtMenu_Attached_0" jax="CHTML" style="font-size: 113.1%; position: relative;" tabindex="0" ctxtmenu_counter="0"><mjx-math class="MJX-TEX" aria-hidden="true"><mjx-msqrt><mjx-sqrt><mjx-surd><mjx-mo class="mjx-n"><mjx-c class="mjx-c221A"></mjx-c></mjx-mo></mjx-surd><mjx-box style="padding-top: 0.174em;"><mjx-mn class="mjx-n"><mjx-c class="mjx-c32"></mjx-c></mjx-mn></mjx-box></mjx-sqrt></mjx-msqrt></mjx-math><mjx-assistive-mml unselectable="on" display="inline"><mjx-container class="MathJax CtxtMenu_Attached_0" jax="CHTML" style="font-size: 113.1%; position: relative;" tabindex="0" ctxtmenu_counter="15"><mjx-math class="MJX-TEX" aria-hidden="true"><mjx-msqrt><mjx-sqrt><mjx-surd><mjx-mo class="mjx-n"><mjx-c class="mjx-c221A"></mjx-c></mjx-mo></mjx-surd><mjx-box style="padding-top: 0.174em;"><mjx-mn class="mjx-n"><mjx-c class="mjx-c32"></mjx-c></mjx-mn></mjx-box></mjx-sqrt></mjx-msqrt></mjx-math><mjx-assistive-mml unselectable="on" display="inline"><math xmlns="http://www.w3.org/1998/Math/MathML"><msqrt><mn>2</mn></msqrt></math></mjx-assistive-mml></mjx-container></mjx-assistive-mml></mjx-container></span></td>
    </tr>
    <tr>
    <td align="center">1</td>
    <td align="center">2</td>
    <td align="center">3</td>
    </tr>

    </tbody>
    </table>
    ```

:warning: It's up to you, to later style/use these classes for this table in your CSS/javascript

### Adding ID to Tables

=== "Rendering"
    |A|B|C|
    |:-:|:-:|:-:|
    | a | b | $\sqrt 2$ |
    | 1 | 2 | 3 |
    { #myid }
=== "Markdown :fa-markdown:"
    ```markdown
    |A|B|C|
    |:-:|:-:|:-:|
    | a | b | $\sqrt 2$ |
    | 1 | 2 | 3 |
    { #myid }
    ```
=== "HTML created :fa-html5:"
    adds ID `myid` to the rendered HTML `table` element :  

    ```html
    <table id="myid">
    <thead>
    <tr>
    <th align="center">A</th>
    <th align="center">B</th>
    <th align="center">C</th>
    </tr>
    </thead>
    <tbody>
    <tr>
    <td align="center">a</td>
    <td align="center">b</td>
    <td align="center"><span class="arithmatex"><mjx-container class="MathJax CtxtMenu_Attached_0" jax="CHTML" style="font-size: 113.1%; position: relative;" tabindex="0" ctxtmenu_counter="1"><mjx-math class="MJX-TEX" aria-hidden="true"><mjx-msqrt><mjx-sqrt><mjx-surd><mjx-mo class="mjx-n"><mjx-c class="mjx-c221A"></mjx-c></mjx-mo></mjx-surd><mjx-box style="padding-top: 0.174em;"><mjx-mn class="mjx-n"><mjx-c class="mjx-c32"></mjx-c></mjx-mn></mjx-box></mjx-sqrt></mjx-msqrt></mjx-math><mjx-assistive-mml unselectable="on" display="inline"><mjx-container class="MathJax CtxtMenu_Attached_0" jax="CHTML" style="font-size: 113.1%; position: relative;" tabindex="0" ctxtmenu_counter="16"><mjx-math class="MJX-TEX" aria-hidden="true"><mjx-msqrt><mjx-sqrt><mjx-surd><mjx-mo class="mjx-n"><mjx-c class="mjx-c221A"></mjx-c></mjx-mo></mjx-surd><mjx-box style="padding-top: 0.174em;"><mjx-mn class="mjx-n"><mjx-c class="mjx-c32"></mjx-c></mjx-mn></mjx-box></mjx-sqrt></mjx-msqrt></mjx-math><mjx-assistive-mml unselectable="on" display="inline"><math xmlns="http://www.w3.org/1998/Math/MathML"><msqrt><mn>2</mn></msqrt></math></mjx-assistive-mml></mjx-container></mjx-assistive-mml></mjx-container></span></td>
    </tr>
    <tr>
    <td align="center">1</td>
    <td align="center">2</td>
    <td align="center">3</td>
    </tr>

    </tbody>
    </table>
    ```

:warning: It's up to you, to later style/use this ID for this table in your CSS/javascript

!!! note
    If several ids are passed in the `{ #myid1 #myid2 }` declaration string, ONLY THE LAST ONE will be retained (here only `#myid2`).  
    (This is normal HTML convention : only one ID is possible for an HTML element.)

### Adding Attributes to Tables

For example, to add the list `{ attr1="hello" attr2="world" }` of `(attribute, value)` pairs to a markdown table.

=== "Rendering"
    |A|B|C|
    |:-:|:-:|:-:|
    | a | b | $\sqrt 2$ |
    | 1 | 2 | 3 |
    { attr1="hello" attr2="world" }
=== "Markdown :fa-markdown:"
    ```markdown
    |A|B|C|
    |:-:|:-:|:-:|
    | a | b | $\sqrt 2$ |
    | 1 | 2 | 3 |
    { attr1="hello" attr2="world" }
    ```
=== "HTML created :fa-html5:"
    adds ID `myid` to the rendered HTML `table` element :  

    ```html
    <table attr1="hello" attr2="world">
    <thead>
    <tr>
    <th align="center">A</th>
    <th align="center">B</th>
    <th align="center">C</th>
    </tr>
    </thead>
    <tbody>
    <tr>
    <td align="center">a</td>
    <td align="center">b</td>
    <td align="center"><span class="arithmatex"><mjx-container class="MathJax CtxtMenu_Attached_0" jax="CHTML" style="font-size: 119.4%; position: relative;" tabindex="0" ctxtmenu_counter="2"><mjx-math class="MJX-TEX" aria-hidden="true"><mjx-msqrt><mjx-sqrt><mjx-surd><mjx-mo class="mjx-n"><mjx-c class="mjx-c221A"></mjx-c></mjx-mo></mjx-surd><mjx-box style="padding-top: 0.174em;"><mjx-mn class="mjx-n"><mjx-c class="mjx-c32"></mjx-c></mjx-mn></mjx-box></mjx-sqrt></mjx-msqrt></mjx-math><mjx-assistive-mml unselectable="on" display="inline"><mjx-container class="MathJax CtxtMenu_Attached_0" jax="CHTML" style="font-size: 119.6%; position: relative;" tabindex="0" ctxtmenu_counter="18"><mjx-math class="MJX-TEX" aria-hidden="true"><mjx-msqrt><mjx-sqrt><mjx-surd><mjx-mo class="mjx-n"><mjx-c class="mjx-c221A"></mjx-c></mjx-mo></mjx-surd><mjx-box style="padding-top: 0.174em;"><mjx-mn class="mjx-n"><mjx-c class="mjx-c32"></mjx-c></mjx-mn></mjx-box></mjx-sqrt></mjx-msqrt></mjx-math><mjx-assistive-mml unselectable="on" display="inline"><math xmlns="http://www.w3.org/1998/Math/MathML"><msqrt><mn>2</mn></msqrt></math></mjx-assistive-mml></mjx-container></mjx-assistive-mml></mjx-container></span></td>
    </tr>
    <tr>
    <td align="center">1</td>
    <td align="center">2</td>
    <td align="center">3</td>
    </tr>

    </tbody>
    </table>
    ```

:warning: It's up to you, to later style/use these attributes for this table in your CSS/javascript

!!! note
    If an attribute **with the same name** is passed several times in the declaration string, e.g. `{ attr1="hello" attr1="world" }`, ONLY THE LAST ONE will be retained (here only `attr1="world"`).  
    (This is normal HTML convention : only one attribute **with the same name** can be used for an HTML element.)

## Shrink a Table

You can use any of the following classes to shrink the table, with **no padding** inside markdown cells :

* `{ .shrink }` or 
* `{ .small }` or 
* `{ .petit }`

=== "Rendering"
    |A|B|C|
    |:-:|:-:|:-:|
    | a | b | $\sqrt 2$ |
    | > | 1 | 3 |
    { .small }
=== "Markdown :fa-markdown:"
    ```markdown
    |A|B|C|
    |:-:|:-:|:-:|
    | a | b | $\sqrt 2$ |
    | > | 1 | 3 |
    { .small }
    ```

## No-Color Table

Massilia Tables come with some predefined & subjective light-dark theme for markdown tables.
You can however use the `{ .nocolor }` class to **overwrite massilia default and apply NO COLOR (transparent cells) styling** for tables.

=== "Rendering"
    |A|B|C|
    |:-:|:-:|:-:|
    | a | b | $\sqrt 2$ |
    | > | 1 | 3 |
    { .nocolor }
=== "Markdown :fa-markdown:"
    ```markdown
    |A|B|C|
    |:-:|:-:|:-:|
    | a | b | $\sqrt 2$ |
    | > | 1 | 3 |
    { .nocolor }
    ```

!!! note
    You can mix the `{.nocolor}` class with the `{.small}` class

    === "NoColor AND Small Rendering"
        |A|B|C|
        |:-:|:-:|:-:|
        | a | b | $\sqrt 2$ |
        | > | 1 | 3 |
        { .nocolor .small }
    === "NoColor AND Small Markdown :fa-markdown:"
        ```markdown
        |A|B|C|
        |:-:|:-:|:-:|
        | a | b | $\sqrt 2$ |
        | > | 1 | 3 |
        { .nocolor .small }
        ```

## Sortable Tables with `{ .sortable }` or `{ .triable }` class, etc..

### Sorting Tables with Markdown Syntax

Massilia `XTables` sorting functionnality makes markdown tables **sortable**, with a simple insertion of one of the following classes (any of them), to the markdown table :

* `{.sort}` 
* `{.sortable}` 
* `{.triable}` 
* `{.tri}` 
* `{.trier}` 

Note : The latter class names can be customised in the `massilia-tablesort-config.js` file.  

Massilia markdown `xtables` sorting functionnality is an **integration** of [:fa-github: tristen/tablesort](https://github.com/tristen/tablesort) javascript sorting (as of version 5.2.1) within mkdocs and markdown tables. For more details on which sorting types are supported (all are activated by default, in massilia xtables), please consider having a look to :

* The Github repo : [:fa-github: tristen/tablesort](https://github.com/tristen/tablesort)
* The Demo page : [https://tristen.ca/tablesort/demo/](https://tristen.ca/tablesort/demo/)

=== "Rendering"
    |Age|Date|Fruit| Price<br/>(`€`, `£` or `$`) | Filesize | Version |
    |:-:|:-:|:-:|:-:|:-:|:-:|
    | 2.6 | 10/11/2021 | Myrtilles | €0.63 | 124k | 31.0.150.57 |
    | 8 | 10/12/2021 | Marrons | €5.50 | 134.56 GB | 11.0.1 |
    | 2 | 13/11/2021 | Framboises | €12.09| 134 B | 18.0.18.43|
    | 9.05 | 27/09/2021 | Crêpes | €1.26| 20 MB | 3.0.0 |
    | 2.3 | 10/11/2024 | Fraises | €0.79 | 4.13 TiB | 3.0.2 |
    | 7 | 8/11/2022 | Kiwis | €2.78 | 19.4 K | 3.1.2 |
    | 4 | 10/11/2020 | Muffins | €4.73 | 12 YB | 3.2.0 |
    { .sortable }
=== "Markdown :fa-markdown:"
    ```markdown
    |Age|Date|Fruit| Price<br/>(`€`, `£` or `$`) | Filesize | Version |
    |:-:|:-:|:-:|:-:|:-:|:-:|
    | 2.6 | 10/11/2021 | Myrtilles | €0.63 | 124k | 31.0.150.57 |
    | 8 | 10/12/2021 | Marrons | €5.50 | 134.56 GB | 11.0.1 |
    | 2 | 13/11/2021 | Framboises | €12.09| 134 B | 18.0.18.43|
    | 9.05 | 27/09/2021 | Crêpes | €1.26| 20 MB | 3.0.0 |
    | 2.3 | 10/11/2024 | Fraises | €0.79 | 4.13 TiB | 3.0.2 |
    | 7 | 8/11/2022 | Kiwis | €2.78 | 19.4 K | 3.1.2 |
    | 4 | 10/11/2020 | Muffins | €4.73 | 12 YB | 3.2.0 |
    { .sortable }
    ```

!!! note
    `tablesort.js` HTML Tables can still be made sortable and inserted directly within an `.md` file in mkdocs, independently of this massilia-xtables integration, provided you add a `class="sortable"` (etc..) attribute to the HTML table. Please consider having a look to tristen's documentation.

    === "Rendering"
        <table class="sortable">
        <thead>
            <tr>
            <th>Number</th>
            <th data-sort-method='dotsep'>Version</th>
            </tr>
        </thead>
        <tbody>
            <tr>
            <td>1</td>
            <td>3.7.2004</td>
            </tr>
            <tr>
            <td>2</td>
            <td>1.08.2013</td>
            </tr>
        </tbody>
        </table>
    === ":fa-html5: HTML"
        ```markdown
        <table class="sortable">
        <thead>
            <tr>
            <th>Number</th>
            <th data-sort-method='dotsep'>Version</th>
            </tr>
        </thead>
        <tbody>
            <tr>
            <td>1</td>
            <td>3.7.2004</td>
            </tr>
            <tr>
            <td>2</td>
            <td>1.08.2013</td>
            </tr>
        </tbody>
        </table>
        ```

### Sorting Tables with HTML Syntax

You can use HTML Syntax for tables to use advanced sorting.  

!!! note
    Please note that :

    * `massilia XTables` uses the [tristen/tablesort.js](https://github.com/tristen/tablesort) library from **:fa-github: Github**, that has been integrated directly into `massilia.js`
    * The complete `tablesort.js` library AND any additional plugin have **already been automatically added** to `massilia.js` (no need to double add them). More precisely, the following have been integrated within `massilia.js` :
        * `tablesort.min.js`
        * `tablesort.number.min.js`
        * `tablesort.date.min.js`
        * `tablesort.dotsep.min.js`
        * `tablesort.monthname.min.js`
        * `tablesort.filesize.min.js`
    * Please consider having a look to [tristen/tablesort.js](https://github.com/tristen/tablesort) syntax for advanced (html?) usage

#### HTML Table with `class='sort'`

=== "Rendering"
    <table class='sort'>
    <thead>
    <tr>
        <th >Product</th>
        <th colspan="2" data-sort-column-key="price">Price</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>Apples</td>
        <td>Sale!</td>
        <td data-sort-column-key="price">20</td>
    </tr>
    <tr>
        <td>Bread</td>
        <td>Out of stock</td>
        <td data-sort-column-key="price">10</td>
    </tr>
    <tr>
        <td>Radishes</td>
        <td>In Stock!</td>
        <td data-sort-column-key="price">30</td>
    </tr>
    </tbody>
    </table>
=== "HTML :fa-html5:"
    ```html
    <table class='sort'>
    <thead>
    <tr>
        <th >Product</th>
        <th colspan="2" data-sort-column-key="price">Price</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>Apples</td>
        <td>Sale!</td>
        <td data-sort-column-key="price">20</td>
    </tr>
    <tr>
        <td>Bread</td>
        <td>Out of stock</td>
        <td data-sort-column-key="price">10</td>
    </tr>
    <tr>
        <td>Radishes</td>
        <td>In Stock!</td>
        <td data-sort-column-key="price">30</td>
    </tr>
    </tbody>
    </table>
    ```

#### HTML Grid Table : With `class='tri'`

=== "Rendering"
    <table class='tri'>
    <thead>
    <tr>
    <th colspan="2" rowspan="1">Table Headings
    </th>
    <th colspan="1" rowspan="1">Here
    </th>
    </tr>
    <tr>
    <th colspan="1" rowspan="1">
    Sub
    </th>
    <th colspan="1" rowspan="1">
    Headings
    </th>
    <th colspan="1" rowspan="1">
    Too
    </th>
    </tr>
    </thead>
    <tbody>
    <tr>
    <td colspan="1" rowspan="2">
    <p>cell
    spans
    rows</p>
    </td>
    <td colspan="2" rowspan="1">
    <p>column spanning</p>
    </td>
    </tr>
    <tr>
    <td colspan="1" rowspan="1">
    <p>normal</p>
    </td>
    <td colspan="1" rowspan="1">
    <p>cell</p>
    </td>
    </tr>
    <tr>
    <td colspan="1" rowspan="1">
    <p>multi
    line</p>
    <p>cells
    too</p>
    </td>
    <td colspan="2" rowspan="1">
    <p>cells can be
    <em>formatted</em>
    <strong>paragraphs</strong></p>
    </td>
    </tr>
    </tbody>
    </table>
=== "HTML :fa-html5:"
    ```html
        <table class='tri'>
    <thead>
    <tr>
    <th colspan="2" rowspan="1">Table Headings
    </th>
    <th colspan="1" rowspan="1">Here
    </th>
    </tr>
    <tr>
    <th colspan="1" rowspan="1">
    Sub
    </th>
    <th colspan="1" rowspan="1">
    Headings
    </th>
    <th colspan="1" rowspan="1">
    Too
    </th>
    </tr>
    </thead>
    <tbody>
    <tr>
    <td colspan="1" rowspan="2">
    <p>cell
    spans
    rows</p>
    </td>
    <td colspan="2" rowspan="1">
    <p>column spanning</p>
    </td>
    </tr>
    <tr>
    <td colspan="1" rowspan="1">
    <p>normal</p>
    </td>
    <td colspan="1" rowspan="1">
    <p>cell</p>
    </td>
    </tr>
    <tr>
    <td colspan="1" rowspan="1">
    <p>multi
    line</p>
    <p>cells
    too</p>
    </td>
    <td colspan="2" rowspan="1">
    <p>cells can be
    <em>formatted</em>
    <strong>paragraphs</strong></p>
    </td>
    </tr>
    </tbody>
    </table>
    ```

## Merge Cells with XTables

### Horizontal Merge with `>` symbol

!!! not
    * The `>` character merges the current cell **HORIZONTALLY, with the contigüous RIGHT cell**
    * Several contigüous `>` may be used to right-merge several cells

=== "Rendering"
    |A|>|B|
    |:-:|:-:|:-:|
    | a | b | $\sqrt 2$ |
    | c |  | d |
    |   |  | e |
    | > |  | f |
    | a | > |  |
    | > | 1 | 3 |
    | > | > | 4 |
=== "Markdown :fa-markdown:"
    ```markdown
    |A|>|B|
    |:-:|:-:|:-:|
    | a | b | $\sqrt 2$ |
    | c |  | d |
    |   |  | e |
    | > |  | f |
    | a | > |  |
    | > | 1 | 3 |
    | > | > | 4 |
    ```

!!! note
    Empty Cells are NOT automatically being merged horizontally. This is meant on purpose.

### Vertical Merge with `^` symbol

!!! not
    * The `^` character merges the current cell **VERTICALLY, with the contigüous UPPER cell**
    * Several contigüous `^` may be used to upper-merge several cells

!!! exp "First Example of Merging Cells with XTables"
    === "Rendering"
        |A|>|Hey|C|D|E|F|G|
        |:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
        | a | b | c | > | $\sqrt 2$ | > | $\pi$ | e |
        | ^ | f | g |   | h     | > | $\sqrt 2$ | i |
        | > | 1 | 4 |     3     |   | > | ^     | ^ |
        | > | ^ | 2 |     9     | 5 |   |       |   |
        | > | ^ | ^ |     5     | ^ |   |       |   |
        | > | ^ | ^ |     6     | 7 |   |       |   |
    === "Markdown :fa-markdown:"
        ```markdown
        |A|>|Hey|C|D|E|F|G|
        |:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
        | a | b | c | > | $\sqrt 2$ | > | $\pi$ | e |
        | ^ | f | g |   | h     | > | $\sqrt 2$ | i |
        | > | 1 | 4 |     3     |   | > | ^     | ^ |
        | > | ^ | 2 |     9     | 5 |   |       |   |
        | > | ^ | ^ |     5     | ^ |   |       |   |
        | > | ^ | ^ |     6     | 7 |   |       |   |
        ```     
    === "Rendering Compatible with `{.nocolor}`"
        |A|>|Hey|C|D|E|F|G|
        |:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
        | a | b | c | > | $\sqrt 2$ | > | $\pi$ | e |
        | ^ | f | g |   | h     | > | $\sqrt 2$ | i |
        | > | 1 | 4 |     3     |   | > | ^     | ^ |
        | > | ^ | 2 |     9     | 5 |   |       |   |
        | > | ^ | ^ |     5     | ^ |   |       |   |
        | > | ^ | ^ |     6     | 7 |   |       |   |
        {.nocolor}
    === "Markdown :fa-markdown: with `{.nocolor}`"
        ```markdown
        |A|>|Hey|C|D|E|F|G|
        |:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
        | a | b | c | > | $\sqrt 2$ | > | $\pi$ | e |
        | ^ | f | g |   | h     | > | $\sqrt 2$ | i |
        | > | 1 | 4 |     3     |   | > | ^     | ^ |
        | > | ^ | 2 |     9     | 5 |   |       |   |
        | > | ^ | ^ |     5     | ^ |   |       |   |
        | > | ^ | ^ |     6     | 7 |   |       |   |
        {.nocolor}
        ```

!!! exp "Second Example of Merging Cells with XTables"
    === "Rendering"
        |>| |>|>|Signe de $\Delta$|
        |:-:|:-:|:-:|:-:|:-:|
        |    >     |  | $\Delta>0$ | $\Delta=0$ | $\Delta<0$ |
        |Signe de a| $a>0$ | :fa-vest: |:fa-vest: | :fa-vest: |
        |^|^|>|>|:fa-vest:|
        |^         | $a<0$ | :fa-vest: | :fa-vest: | :fa-vest: |
        |^|^|>|>| :fa-vest: |
        |>|Nombre de Points<br/>d'intersection| :fa-vest: | :fa-vest: | :fa-vest: |
    === "Markdown :fa-markdown:"
        ```markdown
        |>| |>|>|Signe de $\Delta$|
        |:-:|:-:|:-:|:-:|:-:|
        |    >     |  | $\Delta>0$ | $\Delta=0$ | $\Delta<0$ |
        |Signe de a| $a>0$ | :fa-vest: |:fa-vest: | :fa-vest: |
        |^|^|>|>|:fa-vest:|
        |^         | $a<0$ | :fa-vest: | :fa-vest: | :fa-vest: |
        |^|^|>|>| :fa-vest: |
        |>|Nombre de Points<br/>d'intersection| :fa-vest: | :fa-vest: | :fa-vest: |
        ```

!!! note
    Empty Cells are NOT automatically being merged vertically. This is meant on purpose.

## Understanding Colors in XTables

### What Colors can be used in XTables ?

A color, as for `Massilia.js` project is concerned, can be defined as :

* Any **Standard HTML Color Name** which list can be found on [this W3 page](https://www.w3schools.com/tags/ref_colornames.asp). 
* Any **Hexa Color Code** (WITHOUT the `#` sign, if defined on `mkdocs.yml`)

You can also **customise new color names**, in two different ways :

* in the [`htmlColors.js`](https://github.com/rod2ik/cdn/blob/main/mkdocs/javascripts/htmlColors.js) file. If you wish to do so, you can download a local copy of `htmlColors.js` to extend/customise it manually and locally on your machine. Note that this choice will force you to modify some configs in the code manually. Or joint the project.. add ask for merge requests, or,
    * the file `htmlColors.js` contains all **W3 Standard HTML Color Names**
    * and **some customised ones** (in french)
* in your `mkdocs.yml` file with a specific syntax

The chosen color, can then be applied, both to **Light** and **Dark** modes, **ON A PER-CELL BASIS**, to :

* Any Markdown XTables Line/Row
* Any Markdown XTables Column
* Any Markdown Xtables Cell
* or Globally, to the entire Markdown XTable

### Customisation of the Colors

#### Customization in `htmlColors.js` file

The `htmlColors.js` file looks like this :

```javascript
let htmlColors =  {
  //////////////////////////////////////////////////////////////////////////////////////
  /*                             Standard HTML Colors                                 */
  //////////////////////////////////////////////////////////////////////////////////////
  // colorKey:                [BgLight,   BgdDark, BorderLight, BorderDark, TextLight, TextDark]
  "aliceblue":                ["#f0f8ff", "#f0f8ff", "#ff0000", "#00ffff","#000000","#ffffff"],
  "antiquewhite":             ["#faebd7", "#faebd7", "#ff0000", "#00ffff","#000000","#ffffff"],
  "aqua":                     ["#00ffff", "#00ffff", "#ff0000", "#00ffff","#000000","#ffffff"],
  "aquamarine":               ["#7fffd4", "#7fffd4", "#ff0000", "#00ffff","#000000","#ffffff"],
  "azure":                    ["#f0ffff", "#f0ffff", "#ff0000", "#00ffff","#000000","#ffffff"],
  "beige":                    ["#f5f5dc", "#f5f5dc", "#ff0000", "#00ffff","#000000","#ffffff"],
  ...
  //////////////////////////////////////////////////////////////////////////////////////
  /*                              Custom Colors                                       */
  //////////////////////////////////////////////////////////////////////////////////////
  // colorKey:                [BgLight,     BgDark,  BorderLight, BorderDark, TextLight, TextDark]
  "gris":                     ["#dcdcdc99", "#808080", "#00000099", "#bebebeb3","#000000","#ffffff"],
  "bleu":                     ["#00e4ff99", "#00e4ffcc", "#00a1b499", "#59edff99","#000000","#ffffff"],
  "vert":                     ["#7fff00", "#7fff00ee", "#3b7800", "#daffb6","#000000","#ffffff"],
  "jaune":                    ["#ffff00", "#ffff00cc", "#b0b000", "#fdfd75","#000000","#ffffff"],
  "test":                     ["#ff0000", "#00ff00", "#b0b000", "#fdfd75","#000000","#ffffff"],
}
```

Each **Color** or **Color Palette**, or **Color Key**, is defined by:

* a `colorName` or `ColorKey` : A customised `colorName` that refers to your custom color *palette*, and
* $6$ color parameters :
    * `BgLight` : Background Color for Light Theme
    * `BgDark` :  Background Color for Dark Theme
    * `BorderLight` (only for *badges* currently) : Border Color (of cells) for Light Theme
    * `BorderDark`  (only for *badges* currently) :  Border Color (of cells) for Dark Theme
    * `TextLight` : Text Color for Light Theme
    * `TextDark` :  Text Color for Dark Theme

#### Custom Colors Configuration in `mkdocs.yml`

```yaml
extra:
  massilia:
    colors:
# colorName: BgLight BgDark BorderLight BorderDark  TextLight TextDark
      demo1: ff0000 0000ff transparent transparent 0000ff ff0000
```

### Usage

* `colorName` is a customized name of your (static or dynamic) color which you will use later on
* If `XXX` represents a keyword refering to the zone you want to apply the color to (a line/row, column or cell, cf hereafter, the linecolor keyworkds, columncolor keywords, or cellcolor keywords), the global syntax is :
    * Static : `{XXX = "colorName"}`, e.g. `{XXX = "bleu"}` :
        * `{line = "bleu"}` will apply the static color `demo1` to the current `line` 
        * `{col = "bleu"}` will apply the static color `demo1` to the current `column` 
        * `{cell = "bleu"}` will apply the static color `demo1` to the current `cell` 
    * Dynamic  `{XXX = "@colorName"}`, e.g. `{XXX = "@bleu"}` :
        * `{line = "@bleu"}` will apply the dynamic color `@bleu` to the current `line` 
        * `{col = "@bleu"}` will apply the dynamic color `@bleu` to the current `column` 
        * `{cell = "@bleu"}` will apply the dynamic color `@bleu` to the current `cell` 

### Static Color vs Dynamic Color

* a <bd>STATIC COLOR</bd> (no `@` sign) : Any Official HTML Color Name as shown in [this W3 page](https://www.w3schools.com/tags/ref_colornames.asp)
    * A **Static Color** will render the **Same Background color, `BgLight`, for both Light and Dark Themes**. Text Colors will than be treated with mkdocs defaults (dark text in Light mode, white text in Dark mode)
    * If you use a static color, be aware that some texts may not be very readable (because of default mkdocs choices : black texts in light theme, and white texts in dark themes)
    * Usage : `{XXX = "colorName"}` where
        * `XXX` is a keyword refering to the zone you want to apply the color to (cf hereafter for line/row keywords, column keywords, or cell keywords)
        * `colorName` is an official HTML Standard Color Name, or a customised name
* a <bd>DYNAMIC COLOR</bd> (with a `@` sign) :
    * A **Dynamic Color** (background colors and text colors) will render **Different Background colors for Light and Dark Themes** :
        * `BgLight` background color in light mode
        * `BgDark` background color in dark mode
        * (`BorderLight` and `BorderDark` : transparent) : **not usable in XTables** (only for badges)
        * `TextLight` text color in light mode
        * `TextDark`  text color in dark mode
    * Usage : `{XXX = "@colorName"}` where
        * `XXX` is a keyword refering to the zone you want to apply the color to (cf hereafter for line/row keywords, column keywords, or cell keywords)
        * `colorName` is an official HTML Standard Color Name, or a customised name

!!! warning
    * Massilia XTables syntax, with a `@` sign for Dynamic Colors, is notoriously incompatible with the mkdocs plugin `pymdownx.magic` that auto-linkyfies github user's, the so-called *magic links* (other plugins of `pymdownx` -others than `pymdownx.magic`- can be used safely without any trouble)  
    * **If you are still using the `pymdownx.magic` plugin**, note that you can still use dynamic colors in massilia XTables, by adding an escape `\` character before the `@` sign :  
    In this case, the syntax would be : `{XXX = "\@colorName"}`

## Color Lines/Rows, Columns & Cells with XTables

### Color Lines/Rows or Headers

To color a **Line** or a **Header** in a Table, just place one of the following `XXX` **keywords** or **attributes**, in **just one cell of the line/row you want to color** (you can choose any cell of the line, but just one cell is enough), with a `colorName` as `attributeValue` :

* `line`
* `ligne`
* `linecolor`
* `line-color`
* `row`
* `rowcolor`
* `row-color`

!!! not "An Example"
    * `{line="blue"}` placed on just one cell of a markdown table **line**, be it any cell of the line - but just one is enough-, will color all the corresponding line (the one that contains the cell), with the **STATIC COLOR** `blue`
    * `{line="@blue"}` placed on just one cell of a markdown table **line**, be it any cell of the line - but just one is enough-, will color all the corresponding line (the one that contains the cell), with the **DYNAMIC COLOR** `blue`

=== "Rendering with `{.nocolor}`"
    | a | b {linecolor="@demo1"} | c | d |
    |:-:|:-:|:-:|:-:|
    | allo1 | allo1 |allo1 |allo1 |
    | allo2 | allo2 {line="@demo3"} | allo2 | allo2 |
    | allo3  | allo3 |allo3  {line="@demo2"} | allo3 |
    | allo4 | allo4 | allo4 | allo4 |
    | allo5 | allo5 | allo5 | allo5 {linecolor="@chartreuse"} |
    | allo6 | allo6 | allo6 | allo6 |
    | allo7 | allo7 | allo7 | allo7 |
    | allo8 {line-color="darkmagenta"} | allo8 | allo8 | allo8 |
    | allo9 {line="gold"} | allo9 | allo9 | allo9 |
    | allo10 | allo10 | allo10 | allo10 |
    {.nocolor}
=== "Markdown :fa-markdown: with `{.nocolor}`"
    ```markdown
    | a | b {linecolor="@demo1"} | c | d |
    |:-:|:-:|:-:|:-:|
    | allo1 | allo1 |allo1 |allo1 |
    | allo2 | allo2 {line="@demo3"} | allo2 | allo2 |
    | allo3  | allo3 |allo3  {line="@demo2"} | allo3 |
    | allo4 | allo4 | allo4 | allo4 |
    | allo5 | allo5 | allo5 | allo5 {linecolor="@chartreuse"} |
    | allo6 | allo6 | allo6 | allo6 |
    | allo7 | allo7 | allo7 | allo7 |
    | allo8 {line-color="darkmagenta"} | allo8 | allo8 | allo8 |
    | allo9 {line="gold"} | allo9 | allo9 | allo9 |
    | allo10 | allo10 | allo10 | allo10 |
    {.nocolor}
    ```
=== "Default Rendering"
    | a | b {linecolor="@demo1"} | c | d |
    |:-:|:-:|:-:|:-:|
    | allo1 | allo1 |allo1 |allo1 |
    | allo2 | allo2 {line="@demo3"} | allo2 | allo2 |
    | allo3  | allo3 |allo3  {line="@demo2"} | allo3 |
    | allo4 | allo4 | allo4 | allo4 |
    | allo5 | allo5 | allo5 | allo5 {linecolor="@chartreuse"} |
    | allo6 | allo6 | allo6 | allo6 |
    | allo7 | allo7 | allo7 | allo7 |
    | allo8 {line-color="darkmagenta"} | allo8 | allo8 | allo8 |
    | allo9 {line="gold"} | allo9 | allo9 | allo9 |
    | allo10 | allo10 | allo10 | allo10 |
=== "Markdown :fa-markdown:"
    ```markdown
    | a | b {linecolor="@demo1"} | c | d |
    |:-:|:-:|:-:|:-:|
    | allo1 | allo1 |allo1 |allo1 |
    | allo2 | allo2 {line="@demo3"} | allo2 | allo2 |
    | allo3  | allo3 |allo3  {line="@demo2"} | allo3 |
    | allo4 | allo4 | allo4 | allo4 |
    | allo5 | allo5 | allo5 | allo5 {linecolor="@chartreuse"} |
    | allo6 | allo6 | allo6 | allo6 |
    | allo7 | allo7 | allo7 | allo7 |
    | allo8 {line-color="darkmagenta"} | allo8 | allo8 | allo8 |
    | allo9 {line="gold"} | allo9 | allo9 | allo9 |
    | allo10 | allo10 | allo10 | allo10 |
    ```

### Color Columns in Tables

To color a **Column** in a Table, just place one of the following `XXX` **keywords** or **attributes**, in **just one cell of the column you want to color** (you can choose any cell of the column, but just one cell is enough), with a `colorName` as `attributeValue` :

* `col`
* `colcolor`
* `col-color`
* `column`
* `colonne`

!!! not "An Example"
    * `{col="blue"}` placed on just one cell of a markdown table **column**, be it any cell of the column - but just one is enough-, will color all the corresponding column (the one that contains the cell), with the **STATIC COLOR** `blue`
    * `{col="@blue"}` placed on just one cell of a markdown table **column**, be it any cell of the column - but just one is enough-, will color all the corresponding column (the one that contains the cell), with the **DYNAMIC COLOR** `blue`

=== "Rendering with `{.nocolor}`"
    | a | b | c | d | e | f | g |
    |:-:|:-:|:-:|:-:|:-:|:-:|:-:|
    | allo1 | allo1 | allo1 | allo1 | allo1 | allo1 | allo1 |
    | allo2 | allo2 {line="@demo5"} | allo2 | allo2 |allo2 | allo2 | allo2 |
    | allo3  | allo3 {col="@demo4"} |allo3 | allo3 |allo3 |allo3 |allo3 |
    | allo4 | allo4 | allo4 | allo4 |allo4 |allo4 |allo4 |
    | allo5 | allo5 | allo5 | allo5 |allo5 |allo5 |allo5 |
    | allo6 | allo6 | allo6 | allo6 | allo6 | allo6 {colcolor="@demo5"} | allo6 |
    | allo7 | allo7 | allo7 | allo7 {col-color="@demo6"} | allo7 | allo7 | allo7 |
    | allo8 {line-color="darkmagenta"} | allo8 | allo8 | allo8 | allo8 | allo8 | allo8 |
    | allo9 {line="gold"} | allo9 | allo9 | allo9 | allo9 | allo9 | allo9 |
    | allo10 | allo10 | allo10 | allo10 | allo10 | allo10 | allo10 {col="blue"} |
    {.nocolor}
=== "Markdown :fa-markdown: with `{.nocolor}`"
    ```markdown
    | a | b | c | d | e | f | g |
    |:-:|:-:|:-:|:-:|:-:|:-:|:-:|
    | allo1 | allo1 | allo1 | allo1 | allo1 | allo1 | allo1 |
    | allo2 | allo2 {line="@demo5"} | allo2 | allo2 |allo2 | allo2 | allo2 |
    | allo3  | allo3 {col="@demo4"} |allo3 | allo3 |allo3 |allo3 |allo3 |
    | allo4 | allo4 | allo4 | allo4 |allo4 |allo4 |allo4 |
    | allo5 | allo5 | allo5 | allo5 |allo5 |allo5 |allo5 |
    | allo6 | allo6 | allo6 | allo6 | allo6 | allo6 {colcolor="@demo5"} | allo6 |
    | allo7 | allo7 | allo7 | allo7 {col-color="@demo6"} | allo7 | allo7 | allo7 |
    | allo8 {line-color="darkmagenta"} | allo8 | allo8 | allo8 | allo8 | allo8 | allo8 |
    | allo9 {line="gold"} | allo9 | allo9 | allo9 | allo9 | allo9 | allo9 |
    | allo10 | allo10 | allo10 | allo10 | allo10 | allo10 | allo10 {col="blue"} |
    {.nocolor}
    ```
=== "Rendering"
    | a | b | c | d | e | f | g |
    |:-:|:-:|:-:|:-:|:-:|:-:|:-:|
    | allo1 | allo1 | allo1 | allo1 | allo1 | allo1 | allo1 |
    | allo2 | allo2 {line="@demo5"} | allo2 | allo2 |allo2 | allo2 | allo2 |
    | allo3  | allo3 {col="@demo4"} |allo3 | allo3 |allo3 |allo3 |allo3 |
    | allo4 | allo4 | allo4 | allo4 |allo4 |allo4 |allo4 |
    | allo5 | allo5 | allo5 | allo5 |allo5 |allo5 |allo5 |
    | allo6 | allo6 | allo6 | allo6 | allo6 | allo6 {colcolor="@demo5"} | allo6 |
    | allo7 | allo7 | allo7 | allo7 {col-color="@demo6"} | allo7 | allo7 | allo7 |
    | allo8 {line-color="darkmagenta"} | allo8 | allo8 | allo8 | allo8 | allo8 | allo8 |
    | allo9 {line="gold"} | allo9 | allo9 | allo9 | allo9 | allo9 | allo9 |
    | allo10 | allo10 | allo10 | allo10 | allo10 | allo10 | allo10 {col="blue"} |
=== "Markdown :fa-markdown:"
    ```markdown
    | a | b | c | d | e | f | g |
    |:-:|:-:|:-:|:-:|:-:|:-:|:-:|
    | allo1 | allo1 | allo1 | allo1 | allo1 | allo1 | allo1 |
    | allo2 | allo2 {line="@demo5"} | allo2 | allo2 |allo2 | allo2 | allo2 |
    | allo3  | allo3 {col="@demo4"} |allo3 | allo3 |allo3 |allo3 |allo3 |
    | allo4 | allo4 | allo4 | allo4 |allo4 |allo4 |allo4 |
    | allo5 | allo5 | allo5 | allo5 |allo5 |allo5 |allo5 |
    | allo6 | allo6 | allo6 | allo6 | allo6 | allo6 {colcolor="@demo5"} | allo6 |
    | allo7 | allo7 | allo7 | allo7 {col-color="@demo6"} | allo7 | allo7 | allo7 |
    | allo8 {line-color="darkmagenta"} | allo8 | allo8 | allo8 | allo8 | allo8 | allo8 |
    | allo9 {line="gold"} | allo9 | allo9 | allo9 | allo9 | allo9 | allo9 |
    | allo10 | allo10 | allo10 | allo10 | allo10 | allo10 | allo10 {col="blue"} |
    ```

!!! note
    * Opacity (hence Transparence) can be used when color names are defined, or Hexa Color Codes, 
    * The intersection color of a cell, between a line and a column, will then be the sum of these $2$ colors, creating a 3rd color for the intersection, which **can** be what what you wish (or not).

### Color Cells in Tables

To color a **Cell** in a Table, just place one of the following `XXX` **keywords** or **attributes**, in **the one cell you want to color**, with a `colorName` as `attributeValue` :

* `cell`
* `celcolor`
* `cellcolor`
* `cel-color`
* `cell-color`

!!! not "An Example"
    * `{cell="blue"}` placed on a specific cell of a markdown table, will color the corresponding cell, with the **STATIC COLOR** `blue`
    * `{cell="@blue"}` placed on a specific cell of a markdown table, will color the corresponding cell, with the **DYNAMIC COLOR** `blue`

=== "Rendering with `{.nocolor}`"
    | a  | b | c {cell="orange"} | d | e {cell="@demo1"} | f | g |
    |:-:|:-:|:-:|:-:|:-:|:-:|:-:|
    | allo1 | allo1 | allo1 {col="@palegreen"} | allo1 | allo1 | allo1 | allo1 |
    | allo2 | allo2 | allo2 {line="@demo4" cell="@demo6"} | allo2 |allo2 | allo2 | allo2 {cell="@demo2"} |
    | allo3  | allo3 |allo3 | allo3 |allo3 |allo3 |allo3 |
    | allo4 | allo4 | allo4 | allo4 |allo4 |allo4 |allo4 {cell="@demo3"} |
    | allo5 | allo5 | allo5 | allo5 |allo5 |allo5 |allo5 |
    | allo6 | allo6 | allo6 {cell="@demo1"} | allo6 | allo6 {colcolor="@demo5"} | allo6 | allo6 {cellcolor="@demo4"}|
    | allo7 | allo7 | allo7 | allo7 | allo7 | allo7 | allo7 |
    | allo8 | allo8 | allo8 {line="red" cell="@demo2"} | allo8 | allo8 | allo8 | allo8 |
    | allo9 | allo9 | allo9 | allo9 | allo9 | allo9 | allo9 |
    | allo10 | allo10 | allo10 | allo10 | allo10 | allo10 | allo10 {col="blue"} |
    {.nocolor}
=== "Markdown :fa-markdown:"
    ```markdown
    | a  | b | c {cell="orange"} | d | e {cell="@demo1"} | f | g |
    |:-:|:-:|:-:|:-:|:-:|:-:|:-:|
    | allo1 | allo1 | allo1 {col="@palegreen"} | allo1 | allo1 | allo1 | allo1 |
    | allo2 | allo2 | allo2 {line="@demo4" cell="@demo6"} | allo2 |allo2 | allo2 | allo2 {cell="@demo2"} |
    | allo3  | allo3 |allo3 | allo3 |allo3 |allo3 |allo3 |
    | allo4 | allo4 | allo4 | allo4 |allo4 |allo4 |allo4 {cell="@demo3"} |
    | allo5 | allo5 | allo5 | allo5 |allo5 |allo5 |allo5 |
    | allo6 | allo6 | allo6 {cell="@demo1"} | allo6 | allo6 {colcolor="@demo5"} | allo6 | allo6 {cellcolor="@demo4"}|
    | allo7 | allo7 | allo7 | allo7 | allo7 | allo7 | allo7 |
    | allo8 | allo8 | allo8 {line="red" cell="@demo2"} | allo8 | allo8 | allo8 | allo8 |
    | allo9 | allo9 | allo9 | allo9 | allo9 | allo9 | allo9 |
    | allo10 | allo10 | allo10 | allo10 | allo10 | allo10 | allo10 {col="blue"} |
    {.nocolor}
    ```

### Priority of Colorings

From Lower to Higher Priority :

* Line/Rows Coloring (Lower priority: Lines are colored first)
* Columns coloring (Columns are colored **after** the lines/rows, supercharging them)
* Cells Coloring (Higher priority: Cells are colored **at last**, supercharging all previous colors)

!!! note
    As already said, Transparency (/Opacity) can be used to supercharge intersections of lines and columns.

### Compatibility with Cell Merge

Table Colouring **is compatible** with Cell Merge.

=== "Rendering with `{.nocolor}`"
    | a {cell="@demo1"} | b | > | d {cell="orange"} | e | f | g |
    |:-:|:-:|:-:|:-:|:-:|:-:|:-:|
    | allo1 | allo1 | allo1 | allo1 | allo1 | allo1 | allo1 |
    | allo2 | allo2  | allo2 | allo2 |allo2 | allo2 | allo2 |
    | allo3 | allo3 |allo3 | > | > |allo3 {cell="@demo2"} |allo3 |
    | allo4 | allo4 | allo4 | ^ | ^ | ^ | allo4 |
    | allo5 | allo5 | allo5 | allo5 |allo5 |allo5 |allo5 |
    | > | > | allo6 {cell="@demo3"} | allo6 | allo6 | allo6 | allo6 |
    | allo7 | allo7 | allo7 | allo7 | allo7 | allo7 | allo7 |
    | allo8 | allo8 | allo8 | allo8 | allo8 {cell="red"} | allo8 | allo8 |
    | allo9 {line="@demo4"} | allo9 | allo9 {col="purple"} | allo9 | ^ | allo9 | allo9 |
    | allo10 | allo10 | allo10 | allo10 | ^ | allo10 | allo10 {col="blue"} |
    {.nocolor}
=== "Markdown :fa-markdown:"
    ```markdown
    | a {cell="\@demo1"} | b | > | d {cell="orange"} | e | f | g |
    |:-:|:-:|:-:|:-:|:-:|:-:|:-:|
    | allo1 | allo1 | allo1 | allo1 | allo1 | allo1 | allo1 |
    | allo2 | allo2  | allo2 | allo2 |allo2 | allo2 | allo2 |
    | allo3 | allo3 |allo3 | > | > |allo3 {cell="yellow"} |allo3 |
    | allo4 | allo4 | allo4 | ^ | ^ | ^ | allo4 |
    | allo5 | allo5 | allo5 | allo5 |allo5 |allo5 |allo5 |
    | > | > | allo6 {cell="green"} | allo6 | allo6 | allo6 | allo6 |
    | allo7 | allo7 | allo7 | allo7 | allo7 | allo7 | allo7 |
    | allo8 | allo8 | allo8 | allo8 | allo8 {cell="red"} | allo8 | allo8 |
    | allo9 {line="yellow"} | allo9 | allo9 {col="purple"} | allo9 | ^ | allo9 | allo9 |
    | allo10 | allo10 | allo10 | allo10 | ^ | allo10 | allo10 {col="blue"} |
    {.nocolor}
    ```

## Understanding Css-Based Styles in XTables

### What Css-Based Styles can be used in XTables ?

A Css-based Style, as for `Massilia.js` **XTables** project is concerned, can be defined as :

* Any customised Style Name (you choose the name) 
* a list of several **(CssProperty, CssValue)** pairs

You can customise new styles names, in two different ways :

* in the [`xTableStyles.js`](https://github.com/rod2ik/cdn/blob/main/mkdocs/javascripts/xTableStyles.js) file. If you wish to do so, you can download a local copy of `xTableStyles.js` to extend/customise it manually and locally on your machine. Note that this choice will force you to modify some configs in the code manually. Or joint the project.. add ask for merge requests, or,
    * the file `xTableStyles.js` contains some (very few) Style Names
* in your `mkdocs.yml` file with a specific syntax

The chosen Css-Based Style can then be applied, both to **Light** and **Dark** modes, **ON A PER-CELL BASIS**, to :

* Any Markdown XTables Line/Row
* Any Markdown XTables Column
* Any Markdown Xtables Cell
* or Globally, to the entire Markdown XTable

### Customisation of the Styles

#### Customisation in `xTableStyles.js` file

The `xTableStyles.js` file looks like this :

```javascript
//////////////////////////////////////////////////////////////////////////////////////
/*                             Custom XTable Styles                               */
//////////////////////////////////////////////////////////////////////////////////////

// xTables Styles Syntax     
// {'nameOfTheStyle1': {
//    'light': { 'cssProperty1': 'CssValue1', 'cssProperty2': 'CssValue1',  ..}, 
//    'dark': { 'cssProperty1': 'CssValue1', 'cssProperty2': 'CssValue1',  ..}
//    },
// etc.. 
// }

let xTableStyles =  {
  'normal': {
    'light': {'font-family': 'Source Code Pro', 'background-color': 'yellow', 'color': 'blue'}, 
    'dark':  {'font-family': 'Source Code Pro', 'background-color': 'green', 'color': 'red'}
    },
  'sevillana': {
    'light': {'font-weight': 'bold', 'font-size': '0.8em', 'font-family': "arial", 'background-color': 'blue', 'color': '#ff0000'}, 
    'dark':  {'font-weight': 'bold', 'font-size': '1.3em', 'font-family': "arial", 'background-color': 'orange', 'color': '#00ffff'}
    },
}
```

Each **Style Name** must have a **Light Style** and a **Dark Style** (mandatory), and hence is defined by:

* a `styleName` : A customised **name** that refers to your custom (Dynamic) Style
* a Css-Style Javascript Object for the **Light Theme**
* a Css-Style Javascript Object for the **Dark Theme**

```javascript
// xTables Style Syntax     
// {'nameOfTheStyle1': {
//    'light': { 'cssProperty1': 'CssValue1', 'cssProperty2': 'CssValue1',  ..}, 
//    'dark': { 'cssProperty1': 'CssValue1', 'cssProperty2': 'CssValue1',  ..}
//    },
// etc.. 
// }
```

You can add as many Styles as you wish, each one having as many CssProperties and corresponding CssValues as you wish. Any Css-Property, an corresponding CssValue can be used.

#### Custom Style Configuration in `mkdocs.yml` 

For example, to define a `test` style :

```yaml
# Customization
extra:
  massilia:
    styles:
      test:
        light:
          font-family: Source Code Pro 
          background-color: purple
          color: red
          border-radius: 20px
          box-shadow : 10px 10px 5px orange
        dark:
          font-family: Source Code Pro
          background-color: cyan
          color: darkblue
          border-radius: 20px
          box-shadow : 10px 10px 5px orange
```

### Usage

* `styleName` is a customized name of your **dynamic style** which you will use later on
* If `XXX` represents a keyword refering to the zone you want to apply the style to (a line/row, column or cell, cf hereafter, the linestyle keyworkds, columnstyle keywords, or cellstyle keywords), the global syntax is :
    * Dynamic `{XXX = "styleName"}`, e.g. `{XXX = "title"}` :
        * `{linestyle = "title"}` will apply the dynamic style `title` to the current `line` 
        * `{colstyle = "title"}` will apply the dynamic style `title` to the current `column` 
        * `{cellstyle = "title"}` will apply the dynamic style `title` to the current `cell` 

### All Styles are Dynamic

Contrary to colors, a Style is, by definition, <red>DYNAMIC</red> mandatorily (no need to add a `@` sign) : Any Style is defined in (a local copy of) the `xTablesStyles.js` file, or in the `mkdocs.yml` config file

## Style Lines/Rows, Columns & Cells with XTables

### Style Lines/Rows or Headers

To apply a style to **EACH INDIVIDUAL CELL** of a **Line/Row** or a **Header** in a Table, just place one of the following `XXX` **keywords** or **attributes**, in **just one cell of the line/row/header you want to style** (you can choose any cell of the line/row/header, but just one cell is enough), with a `styleName` as `attributeValue` :

* `lstyle`
* `linestyle`
* `line-style`
* `rstyle`
* `rowstyle`
* `row-style`
* `styleligne`

!!! not "An Example"
    `{lstyle="title"}` placed on just one cell of a markdown table **line**, be it any cell of the line - but just one is enough-, will style **EACH INDIVIDUAL CELL** of the corresponding line (the one that contains the cell), with the **DYNAMIC STYLE** `title`

=== "Rendering with `{.nocolor}`"
    | a | b {lstyle="title"} | c | d |
    |:-:|:-:|:-:|:-:|
    | allo1 | allo1 |allo1 |allo1 |
    | allo20 | allo20 | allo2 | allo2 |
    | allo3  | allo3 |allo3  {rowstyle="sevillana"} | allo3 |
    | allo4 | allo4 {lstyle="test"} | allo4 | allo4 |
    | allo5 | allo5 | allo5 | allo5 {lstyle="sevillana"} |
    | allo6 | allo6 | allo6 | allo6 |
    | allo7 | allo7 | allo7 | allo7 |
    | allo8 {lstyle="test"} | allo8 | allo8 | allo8 |
    | allo9 {lstyle="normal"} | allo9 | allo9 | allo9 |
    | allo10 | allo10 | allo10 | allo10 |
    {.nocolor}
=== "Markdown :fa-markdown: with `{.nocolor}`"
    ```markdown
    | a | b {lstyle="test"} | c | d |
    |:-:|:-:|:-:|:-:|
    | allo1 | allo1 |allo1 |allo1 |
    | allo20 | allo20 {linestyle="normal"} | allo2 | allo2 |
    | allo3  | allo3 |allo3  {rowstyle="sevillana"} | allo3 |
    | allo4 | allo4 | allo4 | allo4 |
    | allo5 | allo5 | allo5 | allo5 {lstyle="sevillana"} |
    | allo6 | allo6 | allo6 | allo6 |
    | allo7 | allo7 | allo7 | allo7 |
    | allo8 {rstyle="test"} | allo8 | allo8 | allo8 |
    | allo9 {lstyle="normal"} | allo9 | allo9 | allo9 |
    | allo10 | allo10 | allo10 | allo10 |
    {.nocolor}
    ```

### Style Columns in Tables

To apply a style to **EACH INDIVIDUAL CELL** of a **Column** in a Table, just place one of the following `XXX` **keywords** or **attributes**, in **just one cell of the column you want to style** (you can choose any cell of the column, but just one cell is enough), with a `styleName` as `attributeValue` :

* `cstyle`
* `colstyle`
* `col-style`
* `column-style`
* `style-col`

!!! not "An Example"
    `{colstyle="title"}` placed on just one cell of a markdown table **column**, be it any cell of the column - but just one is enough-, will style **EACH INDIVIDUAL CELL** of the corresponding column (the one that contains the cell), with the **DYNAMIC STYLE** `title`

=== "Rendering with `{.nocolor}`"
    | a | b {cstyle="sevillana"} | c | d | e | f |
    |:-:|:-:|:-:|:-:|:-:|:-:|
    | allo1 | allo1 |allo1 |allo1 |allo1 |allo1 |
    | allo20 | allo20 | allo2 | allo2 | allo2 | allo2 |
    | allo3  | allo3 |allo3 {cstyle="test"} | allo3 |allo3 |allo3 |
    | allo4 | allo4 | allo4 | allo4 |allo4 |allo4 |
    | allo5 | allo5 | allo5 | allo5 | allo5 {columnstyle="sevillana"} | allo5 |
    | allo6 {cstyle="normal"} | allo6 | allo6 | allo6 | allo6 | allo6 |
    | allo7 | allo7 | allo7 | allo7 | allo7 |allo7 |
    | allo8 | allo8 | allo8 | allo8 | allo8 |allo8 |
    {.nocolor}
=== "Markdown :fa-markdown: with `{.nocolor}`"
    ```markdown
    | a | b {cstyle="test"} | c | d |
    |:-:|:-:|:-:|:-:|
    | allo1 | allo1 |allo1 |allo1 |
    | allo20 | allo20 {colstyle="normal"} | allo2 | allo2 |
    | allo3  | allo3 |allo3  {columnstyle="sevillana"} | allo3 |
    | allo4 | allo4 | allo4 | allo4 |
    | allo5 | allo5 | allo5 | allo5 {cstyle="sevillana"} |
    | allo6 | allo6 | allo6 | allo6 |
    | allo7 | allo7 | allo7 | allo7 |
    | allo8 {cstyle="test"} | allo8 | allo8 | allo8 |
    | allo9 {cstyle="normal"} | allo9 | allo9 | allo9 |
    | allo10 | allo10 | allo10 | allo10 |
    {.nocolor}
    ```

!!! note
    * Opacity (hence Transparence) colors can be used as part of the Style, especially for lines and columns.
    * The intersection style of a cell, between a line and a column, will then be the sum of the colors of the styles, creating a 3rd color style for the intersection, which **can** be what what you wish (or not).

### Style Cells in Tables

To apply a style to a specific **Cell** in a Table, just place one of the following `XXX` **keywords** or **attributes**, **in the one cell you want to style**, with a `styleName` as `attributeValue` :

* `celstyle`
* `cellstyle`
* `cel-style`
* `cell-style`

!!! not "An Example"
    `{cellstyle="title"}` placed on a specific cell of a markdown table, will style the corresponding cell, with the **DYNAMIC COLOR** `title`

=== "Rendering with `{.nocolor}`"
    | a  | b | c {cellstyle="test"} | d | e {cellstyle="normal"} | f | g |
    |:-:|:-:|:-:|:-:|:-:|:-:|:-:|
    | allo1 {cellstyle="normal"} | allo1 | allo1 {col="@palegreen"} | allo1 | allo1 | allo1 | allo1 |
    | allo2 | allo2 | allo2 | allo2 {cellstyle="normal"} |allo2 | allo2 | allo2 {cellstyle="test"} |
    | allo3  | allo3 |allo3 | allo3 |allo3 |allo3 |allo3 |
    | allo4  {linestyle="test"} | allo4 {cellstyle="sevillana"} | allo4 | allo4 |allo4 |allo4 |allo4 |
    | allo5 | allo5 | allo5 | allo5 |allo5 |allo5 |allo5 |
    | allo6 | allo6 | allo6 {cellstyle="normal"} | allo6 | allo6 {colcolor="@demo6"} | allo6 | allo6 |
    | allo7 | allo7 | allo7 | allo7 | allo7 | allo7 | allo7 |
    | allo8 | allo8 | allo8 {line="red" cell="@demo2"} | allo8 | allo8 | allo8 | allo8 |
    | allo9 | allo9 | allo9 | allo9 | allo9 | allo9 | allo9 |
    | allo10 | allo10 | allo10 | allo10 | allo10 | allo10 | allo10 {col="bisque"} |
    {.nocolor}
=== "Markdown :fa-markdown:"
    ```markdown
    | a  | b | c {cellstyle="test"} | d | e {cellstyle="normal"} | f | g |
    |:-:|:-:|:-:|:-:|:-:|:-:|:-:|
    | allo1 {cellstyle="normal"} | allo1 | allo1 {col="@palegreen"} | allo1 | allo1 | allo1 | allo1 |
    | allo2 | allo2 | allo2 | allo2 {cellstyle="normal"} |allo2 | allo2 | allo2 {cellstyle="test"} |
    | allo3  | allo3 |allo3 | allo3 |allo3 |allo3 |allo3 |
    | allo4  {linestyle="test"} | allo4 {cellstyle="sevillana"} | allo4 | allo4 |allo4 |allo4 |allo4 |
    | allo5 | allo5 | allo5 | allo5 |allo5 |allo5 |allo5 |
    | allo6 | allo6 | allo6 {cellstyle="normal"} | allo6 | allo6 {colcolor="@demo6"} | allo6 | allo6 |
    | allo7 | allo7 | allo7 | allo7 | allo7 | allo7 | allo7 |
    | allo8 | allo8 | allo8 {line="red" cell="@demo2"} | allo8 | allo8 | allo8 | allo8 |
    | allo9 | allo9 | allo9 | allo9 | allo9 | allo9 | allo9 |
    | allo10 | allo10 | allo10 | allo10 | allo10 | allo10 | allo10 {col="bisque"} |
    {.nocolor}
    ```

### Priority of Styles

From Lower to Higher Priority :

* Line/Rows Styling (Lower priority: Lines/Rows are styled first)
* Columns Styling (Columns are styled **after** the lines/rows, supercharging them)
* Cells Styling (Higher priority: Cells are styled **at last**, supercharging all previous styles)

!!! note
    As already said, Transparency (/Opacity) can be used to supercharge (some properties of the) style of intersections of lines and columns.

### Compatibility with Cell Merge

Table Styling **is compatible** with Cell Merge.

=== "Rendering with `{.nocolor}`"
    | a {cellstyle="normal"} | b | > | d {cellstyle="normal"} | e | f | g |
    |:-:|:-:|:-:|:-:|:-:|:-:|:-:|
    | allo1 | allo1 | allo1 | allo1 | allo1 | allo1 | allo1 |
    | allo2 | allo2  | allo2 | allo2 |allo2 | allo2 | allo2 |
    | allo3 | allo3 |allo3 | > | > |allo3 {cellstyle="test"} |allo3 |
    | allo4 | allo4 | allo4 | ^ | ^ | ^ | allo4 |
    | allo5 | allo5 | allo5 | allo5 |allo5 |allo5 |allo5 |
    | > | > | allo6 {cell="test"} | allo6 | allo6 | allo6 | allo6 |
    | allo7 | allo7 | allo7 | allo7 | allo7 | allo7 | allo7 |
    | allo8 | allo8 | allo8 | allo8 | allo8 {cell="red"} | allo8 | allo8 |
    | allo9 {linestyle="sevillana"} | allo9 | allo9 | allo9 | ^ | allo9 | allo9 |
    | allo10 | allo10 | allo10 | allo10 | ^ | allo10 | allo10 {col="blue"} |
    {.nocolor}
=== "Markdown :fa-markdown:"
    ```markdown
    | a {cellstyle="test"} | b | > | d {cellstyle="normal"} | e | f | g |
    |:-:|:-:|:-:|:-:|:-:|:-:|:-:|
    | allo1 | allo1 | allo1 | allo1 | allo1 | allo1 | allo1 |
    | allo2 | allo2  | allo2 | allo2 |allo2 | allo2 | allo2 |
    | allo3 | allo3 |allo3 | > | > |allo3 {cellstyle="normal"} |allo3 |
    | allo4 | allo4 | allo4 | ^ | ^ | ^ | allo4 |
    | allo5 | allo5 | allo5 | allo5 |allo5 |allo5 |allo5 |
    | > | > | allo6 {cell="test"} | allo6 | allo6 | allo6 | allo6 |
    | allo7 | allo7 | allo7 | allo7 | allo7 | allo7 | allo7 |
    | allo8 | allo8 | allo8 | allo8 | allo8 {cell="red"} | allo8 | allo8 |
    | allo9 {linestyle="sevillana"} | allo9 | allo9 | allo9 | ^ | allo9 | allo9 |
    | allo10 | allo10 | allo10 | allo10 | ^ | allo10 | allo10 {col="blue"} |
    {.nocolor}
    ```

## Maths Variation Tables

```vartable
\begin{tikzpicture}
\tikzset{h style/.style = {fill=red!50}}
\tkzTabInit[lgt=1,espcl=2]{$x$ /1, $f$ /2}{$0$,$1$,$2$,$3$}%
\tkzTabVar{+/ $1$ / , -CH/ $-2$ / , +C/ $5$, -/ $0$ / }
\end{tikzpicture}
```

Maths Variation Tables are now available, via the **mkdocs-tex2svg** extension for mkdocs.

For more details, cf :

* [Installation & Configuration of mkocs-tex2svg](https://eskool.gitlab.io/mkhack3rs/maths/tables/)
* [Rendering Examples of mkdocs-tex2svg](https://eskool.gitlab.io/mkhack3rs/maths/tables/examples/)

