# Home

This site focuses on the integration of various technologies and tools with mkdocs, and more especifically with the **material theme** of **mkdocs**.

<env>mkdocs Compatibility</env>

The features described in this site are currently compatible with :  

* mkdocs : 1.4.0
* mkdocs-asy : 1.0
* mkdocs-graphviz : 1.4.87
* mkdocs-kroki-plugin : 0.3.0
* mkdocs-macros-plugin : 0.7.0
* mkdocs-material : 8.5.6
* mkdocs-material-extensions : 1.0.3
* mkdocs-minify-plugin : 0.5.0
* mkdocs-pymdownx-material-extras : 2.2.1
* mkdocs-redirects : 1.2.0

