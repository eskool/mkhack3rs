# LEAFLET

Avec balise ```<center>```

<center>

|a|b|
|:-:|:-:|
|1|2|

</center>

Même Ligne:

|a|b|
|:-:|:-:|
|1|2|{.center}

Ligne suivante:

|a|b|
|:-:|:-:|
|1|2|
{.center}

## Simple Leaflet

<div id="map" style="width: 600px; height: 400px;"></div>

<script>

var map = L.map('map').setView([50.84673, 4.35247], 12);

L.tileLayer('https://tile.openstreetmap.be/osmbe/{z}/{x}/{y}.png', {
     attribution:
         '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors' +
         ', Tiles courtesy of <a href="https://geo6.be/">GEO-6</a>',
     maxZoom: 18
 }).addTo(map);

var marker = L.marker([50.84673, 4.35247]).addTo(map);

var popup = marker.bindPopup('<b>Hello world!</b><br />I am a popup.');

popup.openPopup();

</script>

## Chloropleth Leaflet

<div id="mapC" style="width: 600px; height: 400px;"></div>

<script src="https://leafletjs.com/examples/choropleth/us-states.js"></script>

<script>

var mapboxAccessToken = {pk.eyJ1Ijoicm9kMmlrIiwiYSI6ImNrd2FyczZrajAzb3gycG4ydWYwanJrdWwifQ.e8Rph4erqONHATqd5d8AVw};
var map = L.map('mapC').setView([37.8, -96], 4);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=' + mapboxAccessToken, {
    id: 'mapbox/light-v9',
    attribution: "Rodrigo",
    tileSize: 512,
    zoomOffset: -1
}).addTo(map);

L.geoJson(statesData).addTo(map);

</script>